

def add_depth_noise_ref_hp(d, f):

	for chrom , infos in d.items():

		for position, composition in infos.items():
			# add depth
			ref = f[chrom][position-1]
			d[chrom][position]['ref'] = ref.upper()


			# add homopolymer info
			hp = 1
			# check hp left - max - 20
			for i in range(2, 22):
				x = f[chrom][position-i]
				if x == ref:
					hp += 1
				else:
					break

			# check hp right - max - 20
			for i in range(0, 20):
				x = f[chrom][position+i]
				if x == ref:
					hp += 1
				else:
					break


			d[chrom][position]['HP'] = hp


			# add depth
			depth =  composition['A'][0]+composition['A'][1]
			depth += composition['C'][0]+composition['C'][1]
			depth += composition['G'][0]+composition['G'][1]
			depth += composition['T'][0]+composition['T'][1]

			n_ins = 0
			for value in composition['in'].values():
				n_ins += value[0]+value[1]
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# insertions shouldn't be taken into account when calculating depth ???
			depth += n_ins
			depth += n_del

			d[chrom][position]['depth'] = depth


			# add noise estimation
			# get total base quality scores at this position
			qScore = d[chrom][position]['base_error_probability']
			
			# get depth
			depth = d[chrom][position]['depth']
			
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# substract deletion from depth because deletions are not taken into account
			# in the total qscore since deletions don't have qscore
			depth -= n_del
			# divide by the depth to get mean qscore of the position
			mean_qscore = math.floor(float(qScore)/depth) if depth > 0 else 0
			d[chrom][position]['qScore'] = mean_qscore

			# qScore = -10.log(base_error_probability)
			# use this formula to calculate base_error probability
			base_error_probability = 10**(float(mean_qscore)/-10)
			# add the estimated base_error_probability to the position in the pileup
			d[chrom][position]['base_error_probability'] = round(base_error_probability, 6)

			
			return d



