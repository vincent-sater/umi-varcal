import os
import sys
import time
import math
import msgpack
from pyfaidx import Fasta


# import local modules
from functions import *

# from AddRead import *

def TreatReads(SAM, nReads, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY):

	validReads = 0

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}

	a = {'A' : '1', 'C': '2', 'G':'3', 'T':'4'}

	i=0
	for q in quals_str:
		quals[quals_str[i]] = i
		i += 1


	# pileup = ParseBED(BED)

	with open(OUTPUT+"/.pileup_ini", 'rb') as handle:
		pileup = msgpack.unpack(handle, encoding="utf-8")
		# pileup = pickle.load(handle)
	
	pileup = SortPileup(pileup)


	currentLine = 1.0
	lastProgress = 0.0
	# totalLines = GetTotalLines(SAM)
	totalLines = nReads


	ALL_UMIS = {}

	
	for line in open(SAM):

		lastProgress = PrintProgress(currentLine, totalLines, lastProgress)

		currentLine += 1


		line = line.split('\t')

		readName = line[0].split('_')

		
		try:
			umi = readName[-1]
			readName = "_".join(readName[:-1])
			
			readName = readName.split(':')[-2:]
			readName = list(map(int, readName))
			readName = readName[0]*readName[1]

			# readName = ":".join(readName)




			

		except:
			continue


		try:
			flag = int(line[1])
		except:
			continue
		
		# get strand : 0 = forward | 1 = reverse
		strand = int("{0:b}".format(int(flag))[-5])
		firstInPair = bool(int("{0:b}".format(int(flag))[-7]))
		chrom = line[2]
		pos = line[3]
		mapq = int(line[4])
		cigar = line[5]
		seq = line[9]
		qual = line[10]


		matePos = line[7]
		umiPos = int(str(pos)[-4:]) if strand == 0 else int(str(matePos)[-4:])
		

		ini_umi = umi+"-"+str(umiPos)
		found = False

		for i in range(umiPos - 3, umiPos + 3):
			test_umi = umi+"-"+str(i)
			if test_umi in ALL_UMIS:
				umi = test_umi
				ALL_UMIS[test_umi] += 1
				found = True
				break
		
		if not found:
			umi = umi+"-"+str(umiPos)
			ALL_UMIS[umi] = 1


		if ReadIsValid(flag, chrom, pos, umi, cigar, mapq, MIN_MAPPING_QUALITY, qual, quals, MIN_READ_QUALITY):
			
			AddRead(pileup, readName, umi, strand, chrom, int(pos), cigar, seq, qual, quals, MIN_BASE_QUALITY)
			validReads += 1



	return [pileup, validReads]