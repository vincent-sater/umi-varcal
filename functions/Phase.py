#
# THIS FUNCTION IS USED TO DETECT PHASED VARIANTS AMONG THE FINAL LIST OF DETECTED VARIANTS
# 
# INPUT : 
#         -VARIANTS            : (DICT)   A DICTIONNARY CONTAINING THE FINAL LIST OF THE DETECTED VARIANTS
# 		  -MIN_PHASE_UMI       : (INT)    THRESHOLD FOR THE MINIMUM NUMBER OF COMMON UMI BETWEEN TWO VARIANTS TO BE CONSIDERED PHASED
# 		  -MIN_PHASE_VAF_RATIO : (FLOAT)  THRESHOLD FOR THE MINIMUM VAF RATIO OF TWO VARIANTS TO BE CONSIDERED PHASED
# 		  -MAX_PHASE_DISTANCE  : (INT)    THRESHOLD FOR THE MAXIMUM DISTANCE BETWEEN PHASED VARIANTS
#
#
# VALUE : -PHASEDVARIANTS      : (DICT)   A DICTIONNARY CONTAINING A LIST OF THE DETECTED PHASED VARIANTS AND THEIR INFOS
#

import os
import sys


def Phase(variants, MIN_PHASE_UMI, MIN_PHASE_VAF_RATIO, MAX_PHASE_DISTANCE):



	seen = {}
	candidates = {}

	for x in variants:
		for y in variants[x]:
			chrom = variants[x][y]['line'].split('\t')[0]
			pos = int(variants[x][y]['line'].split('\t')[1])
			var = variants[x][y]['line'].split('\t')[2]
			vaf = float(variants[x][y]['line'].split('\t')[7].split(';')[1].replace('UMI_AF=', ''))
			umi = variants[x][y]['umi']

			try:
				seen[chrom][var] = {'position': pos, 'umi': umi, 'vaf': vaf}
			except:
				seen[chrom] = {var: {'position': pos, 'umi': umi, 'vaf': vaf}}







	for chrom in seen:
		x = seen[chrom]
		for var in seen[chrom]:
			y = list(x.keys())
			y.remove(var)
			pos = seen[chrom][var]['position']
			for var2 in y:
				pos2 = seen[chrom][var2]['position']
				if abs(pos2 - pos) <= MAX_PHASE_DISTANCE:
					try:
						candidates[var][var2] = {'umi1': seen[chrom][var]['umi'], 'umi2': seen[chrom][var2]['umi'], 'vaf1': seen[chrom][var]['vaf'], 'vaf2': seen[chrom][var2]['vaf']}
					except:
						candidates[var] = {var2: {'umi1': seen[chrom][var]['umi'], 'umi2': seen[chrom][var2]['umi'], 'vaf1': seen[chrom][var]['vaf'], 'vaf2': seen[chrom][var2]['vaf']}}






	final = {}





	for x in candidates:

		old_umis_in_common = []

		for y in candidates[x]:

			umi1 = set(candidates[x][y]['umi1'])
			umi2 = set(candidates[x][y]['umi2'])
			vaf1 = candidates[x][y]['vaf1']
			vaf2 = candidates[x][y]['vaf2']
			
			com = len(umi1&umi2)
			ratio = round(vaf1/vaf2, 2) if vaf1 < vaf2 else round(vaf2/vaf1, 2) 

			new_umis_in_common = old_umis_in_common.copy()
			new_umis_in_common.append(umi2)
			cumulative_com_umi = len(set.intersection(*[i for i in new_umis_in_common])) if len(old_umis_in_common) > 0 else com


			# if "23230321" in x and "23230373" in y:
			# 	print(1)
			# 	print(umi1)
			# 	print(umi2)
			# 	# print(umis_in_common)
			# 	print("\n")

			# if "23230321" in x and "23230439" in y:
			# 	print(2)
			# 	print(umi1)
			# 	print(umi2)
			# 	# print(umis_in_common)
			# 	print("\n")

			# if "27087433" in x and "27087409" in y:
			# 	print(3)
			# 	print(umi1)
			# 	print(umi2)
			# 	# print(umis_in_common)
			# 	print("\n")

			
			if com >= MIN_PHASE_UMI:
				if ratio >= MIN_PHASE_VAF_RATIO:
					final[x+" - "+y] =  str(com)+" / "+str(ratio)+" / "+str(cumulative_com_umi)

					old_umis_in_common.append(umi1)
					old_umis_in_common.append(umi2)




	# for x in final:
	# 	if '23230321G>C -' in x:
	# 		print(x)
	# 		print(final[x])




	network = {}

	for x, infos in final.items():
		x = x.split(' - ')
		var1 = x[0]
		var2 = x[1]
		infos = infos.split(' / ')

		try:
			network[var1][var2] = {'com_umi': infos[0], 'vaf_ratio': infos[1], 'cumulative_com_umi': infos[2]}
		except:
			network[var1] = {var2: {'com_umi': infos[0], 'vaf_ratio': infos[1], 'cumulative_com_umi': infos[2]}}



		# for x in network:
		# 	children = network[x]
		# 	print(x, children)
		


	return(network)




