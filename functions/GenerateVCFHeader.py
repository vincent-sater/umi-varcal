
import datetime

def GenerateVCFHeader(INPUT, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, SB_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, VERSION):
	
	date = datetime.datetime.now().strftime("%Y-%m-%d")

	l1 = '##fileformat=VCFv4.2\n'
	l2 = '##fileDate="'+date+'"\n'
	l3 = '##source="UMI-VarCal"\n'
	l4 = '##toolVersion="'+VERSION+'"\n'
	l5 = '##command="input=\''+INPUT+'\', bed=\''+BED+'\', fasta=\''+FASTA+'\', out=\''+OUTPUT+'\', min_base_quality='+str(MIN_BASE_QUALITY)+', min_read_quality='+str(MIN_READ_QUALITY)+', min_mapping_quality='+str(MIN_MAPPING_QUALITY)+', min_variant_umi='+str(MIN_VARIANT_UMI)+', strand_bias_method=\''+str(SB_METHOD)+'\', max_strand_bias='+str(MAX_STRAND_BIAS)+', alpha='+str(ALPHA)+', max_hp_length='+str(MAX_HP_LENGTH)+', cores='+str(CORES)+'\"\n'
	l6 = '##reference="'+FASTA+'"\n'
	rest = """##FILTER=<ID=ID,Description="Generic filter">
##INFO=<ID=AF,Number=1,Type=Float,Description="Allele frequency based on allele count/depth">
##INFO=<ID=UMI_AF,Number=2,Type=Float,Description="Allele frequency based on UMI count/depth">
##INFO=<ID=AO,Number=3,Type=Integer,Description="Alternate allele observation count">
##INFO=<ID=DP,Number=4,Type=Integer,Description="Total read depth at the locus">
##INFO=<ID=HP,Number=5,Type=Integer,Description="Homopolymer length at the locus">
##INFO=<ID=TYPE,Number=6,Type=String,Description="The variation type: either SNV, INS or DEL">
##INFO=<ID=CONF,Number=7,Type=String,Description="Confidence of the variant. It has 5 levels: low(1/5), average(2/5), high(3/5), strong(4/5) or certain(5)">
##INFO=<ID=PHASED,Number=8,Type=List(String),Description="List of phased variants">
##INFO=<ID=PHASED_UMI,Number=9,Type=List(Integer),Description="List of the number of common UMI between variants. Each number represents the number of common UMI between the main variant and the specified phased variant">
##INFO=<ID=PHASED_CUMULATIVE_UMI,Number=10,Type=List(Integer),Description="List of the number of cumulative common UMI between variants. Each number represents the number of common UMI between the main variant, the specified phased variant and all the phased variants occuring before it">
##INFO=<ID=PHASED_VAF_RATIO,Number=11,Type=List(Float),Description="List of the VAF Ratio  with every phased variant">
#CHROM	POS	ID	REF	ALT	Q-VALUE	FILTER	INFO
"""

	return l1+l2+l3+l4+l5+l6+rest
