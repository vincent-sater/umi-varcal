from collections import OrderedDict
from array import array
import os
import re
import sys
import time
import math
import msgpack
import statistics
from multiprocessing import Pool
import datetime
from func import PrintTime, PrintProgress
import operator
from scipy.stats import poisson
import statsmodels.stats.multitest as smm
import pysam
from collections import defaultdict
import psutil


#
# THIS FUNCTION WILL APPLY MULTIPLE FILTERS TO THE VARINATS THAT PASS THE POISSON TEST IN ORDER TO REDUCE FALSE POSITIVES 
# 
# INPUT : 
#         -PILEUP                  : (DICT)   THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		  -F                       : (DICT)   A DICTIONNARY CONTAINING THE REFERENCE BASES AT ALL POSITIONS OF THE GENOME 
# 		  -SB_METHOD               : (STR)    DEFAULT METHOD for SB CALCULATION OR TORRENT SUITE METHOD
# 		  -MAX_STRAND_BIAS         : (FLOAT)  THRESHOLD FOR A VARIANT TO BE CONSIDERED AS STRAND BIASED
# 		  -MIN_VARIANT_UMI         : (INT)    THRESHOLD for A VARIANT WITH A CERTAIN UMI COUNT TO BE CALLED
#         -MIN_VARIANT_DEPTH       : (INT)    THRESHOLD for A VARIANT WITH A CERTAIN UMI DEPTH TO BE CALLED
# 		  -MAX_HP_LENGTH           : (INT)    HOMOPOLYMER REGION LENGTH THRESHOLD FOR A VARIANT IN IT TO BE CALLED 
#		  -BLACK_LIST		       : (LIST)   A LIST CONTAINING THE INDEXES OF SPECIFIC VARIANTS THAT SHOULD NOT BE CALLED EVEN IF
#										      THEY PASS THE FILTERS
#		  -WHITE_LIST		       : (LIST)   A LIST CONTAINING THE INDEXES OF SPECIFIC VARIANTS THAT SHOULD BE CALLED EVEN IF
#										      THEY DON'T PASS THE FILTERS
#         -IGNORE_MONOCLUSTER_UMI  : (BOOL)   IF TRUE, MONOCLUSTER UMI WILL NOT BE CONSIDERED AS CONCORDANT UMI TAGS
#
# VALUE : -FINALVARIANTS           : (DICT)   A DICTIONNARY CONTAINING ONLY THE FINAL VARIANTS THAT SUCCESSFULLY PASSED ALL FILTERS
# VALUE : -phasedVariants          : (DICT)   A DICTIONNARY CONTAINING THE SAME VARIANTS AS IN THE FINAL DICT + INFOS TO ANALYZED THEIR PHASED STATUS
#


def CallVariants(pileup, f, SB_METHOD, MAX_STRAND_BIAS, MIN_VARIANT_UMI, MIN_VARIANT_DEPTH, MAX_HP_LENGTH, BLACK_LIST, WHITE_LIST, IGNORE_MONOCLUSTER_UMI):

	# create a dictionnary to contain only final variants
	finalVariants = OrderedDict()
	phasedVariants = OrderedDict()

	print("\n")
	PrintTime('console', "\tCalling Variants...")


	# define counters to calculate progress
	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetPileupLength(pileup))

	# loop through the PILEUP dictionnary
	# for each chromosome
	for chrom , infos in pileup.items():
		# try-except block for first variants
		# if chromosome already in finalVariants - first variant - dict
		try:
			# do nothing
			test = finalVariants[chrom+"|v"]
		
		# if chromosome not in finalVariants - first variant - dict
		except:
			# add the chromosome to the finalVariants - first variant - dict
			finalVariants[chrom+"|v"] = OrderedDict()
			phasedVariants[chrom+"|v"] = OrderedDict()


		# try-except block for second variants
		# if chromosome already in finalVariants - second variant - dict
		try:
			# do nothing
			test = finalVariants[chrom+"|v2"]
		
		# if chromosome not in finalVariants - second variant - dict
		except:
			# add the chromosome to the finalVariants - second variant - dict
			finalVariants[chrom+"|v2"] = OrderedDict()
			phasedVariants[chrom+"|v2"] = OrderedDict()


		# try-except block for third variants
		# if chromosome already in finalVariants - third variant - dict
		try:
			# do nothing
			test = finalVariants[chrom+"|v3"]
		
		# if chromosome not in finalVariants - third variant - dict
		except:
			# add the chromosome to the finalVariants - third variant - dict
			finalVariants[chrom+"|v3"] = OrderedDict()
			phasedVariants[chrom+"|v3"] = OrderedDict()

		# for each position | composition = counts + infos (ref_allele, alt_allele, q-value, ...)
		for position, composition in infos.items():

			# function that calculates and displays progress
			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			# retrieve list of reference UMIs from PILEUP dictionnary
			total_ref_umi = composition[composition['ref']][2]
			ref_umi_len = len(total_ref_umi)
			ref_umi = list(set(composition[composition['ref']][2]))


			# make list to contain all the possible alternative alleles
			alt_list = [composition['alt']]
			# another list to contain the indexes ["", "2", "3"]
			alt_indexes = {composition['alt']: ""}

			if composition['alt2'] != None:
				alt_list.append(composition['alt2'])
				alt_indexes[composition['alt2']] = "2"
			if composition['alt3'] != None:
				alt_list.append(composition['alt3'])
				alt_indexes[composition['alt3']] = "3"



			# for each possible alternative allele 
			for alt_element in alt_list:

				# get corresponding index
				alt_index = alt_indexes[alt_element]


				# retrieve list of variant UMIs from PILEUP dictionnary
				# if variant is insertion or deletion
				if alt_element == 'in' or alt_element == 'del':

					
					umi_list = []
					readNames = []

					# for each insertion or deletion
					for indel, infos in composition[alt_element].items():
						if isinstance(infos, list):
							# append the UMI to the umi list
							umi_list += infos[2]
							# append the readName to the readNames list
							readNames += infos[3]

				# else if variant is substitution
				else:
					# retrieve list of variant UMIs from PILEUP dictionnary
					umi_list = composition[alt_element][2]
					# retrieve list of readNames from PILEUP dictionnary
					readNames = composition[alt_element][4]


				# get separate true UMI singletons from paired UMI singletons
				# true UMI singleton is a UMI that was read a single time on R1 or R2
				# paired singleton is a UMI that was found on a paired-end R1 and R2 reads only
				# get list of unique alt UMIs 
				value = TreatSingletons(umi_list, readNames, IGNORE_MONOCLUSTER_UMI)
				alt_umi = value[0]
				n_singleton_UMI = value[1] 
				n_monocluster_UMI = value[2]
				n_polycluster_UMI = value[3]

				
				# create the list of unique noise UMIs
				# noise UMIs are the UMIs found on reads that had neither the reference base nor the variant
				alphabet = ["A", "C", "G", "T", "in", "del"]
				alphabet.remove(composition['ref'])
				alphabet.remove(alt_element)
				noise_umi = set()
				for c in alphabet:
					if c == 'in' or c == 'del':
						for indel, infos in composition[c].items():
							if isinstance(infos, list):
								noise_umi.update(infos[2])
					else:
						noise_umi.update(composition[c][2])




				#calculate total + coverage
				pos_covTotal = composition['A'][0]+composition['C'][0]+composition['G'][0]+composition['T'][0]
				
				#calculate total - coverage
				neg_covTotal = composition['A'][1]+composition['C'][1]+composition['G'][1]+composition['T'][1]

				# add insertion + and - coverage to total + and - coverage
				for indel, infos in composition['in'].items():
					if isinstance(infos, list):
						pos_covTotal += infos[0]
						neg_covTotal += infos[1]

				# add deletion + and - coverage to total + and - coverage
				for indel, infos in composition['del'].items():
					pos_covTotal += infos[0]
					neg_covTotal += infos[1]

				# retrieve variant + and - coverage
				# if variant is insertion or deletion => go through all insertions / deletions 
				if alt_element == 'in' or alt_element == 'del':
					pos_covAllele = 0
					neg_covAllele = 0
					for indel, infos in composition[alt_element].items():
						if isinstance(infos, list):
							pos_covAllele += infos[0]
							neg_covAllele += infos[1]

				# else if variant is substitution, retrieve from PILEUP dictionnary
				else:
					pos_covAllele = composition[alt_element][0]
					neg_covAllele = composition[alt_element][1]

				

				#Strand bias computation ( we add 1 to avoid 0 values)
				Vp=float(pos_covAllele+1)
				Vm=float(neg_covAllele+1)
				
				Cp=float(pos_covTotal+1)
				Cm=float(neg_covTotal+1)
				
				SB = CalculateStrandBias(Cp, Cm, Vp, Vm, SB_METHOD)



				# ref discordant UMIs are UMIs that are found in the reference UMIs list
				# AND in the (variant|noise) UMIs list
				# ref concordant UMIs are UMIs that are found in the reference UMIs list ONLY
				# ref_discordant = 0
				# for umi in ref_umi:
				# 	if umi in alt_umi or umi in noise_umi:
				# 		ref_discordant += 1

				# ref_concordant = len(ref_umi) - ref_discordant


				# alt discordant UMIs are UMIs that are found in the variant UMIs list
				# AND in the (reference|noise) UMIs list
				# alt concordant UMIs are UMIs that are found in the variant UMIs list ONLY
				# alt_discordant = 0
				# for umi in alt_umi:
				# 	if umi in ref_umi or umi in noise_umi:
				# 		alt_discordant += 1

				# alt_concordant = len(alt_umi) - alt_discordant



				alt_concordant_umi = []
				for umi in alt_umi:
					if umi not in ref_umi and umi not in noise_umi:
						alt_concordant_umi.append(umi)

				alt_concordant = len(alt_concordant_umi)
				alt_discordant = len(alt_umi) - alt_concordant



				###########################################################################
				##################   FOR TESTING PURPOSES - START   #######################
				###########################################################################

				# print(ref_umi)
				# print(alt_umi)
				# print(noise_umi)
				# print "posCovAllele : "+str(pos_covAllele)
				# print "negCovAllele : "+str(neg_covAllele)
				# print "posCovTotal : "+str(pos_covTotal)
				# print "negCovTotal : "+str(neg_covTotal)
				# print "Strand bias : "+str(SB)
				# print "ref_concordant : "+str(ref_concordant)
				# print "ref_discordant : "+str(ref_discordant)
				# print "alt_concordant : "+str(alt_concordant)
				# print "alt_discordant : "+str(alt_discordant)


				# if chrom == "chr1" and position == 27106356:
				# 	print "\n"
				# 	print alt_umi
				# 	print len(alt_umi)
				# 	print alt_discordant
				# 	print alt_concordant
				# 	print SB
				# 	exit()

				###########################################################################
				###################   FOR TESTING PURPOSES - END   ########################
				###########################################################################

				# add alt_concordant and SB informations to pileup
				pileup[chrom][position]['alt_concordant'+alt_index] = alt_concordant
				pileup[chrom][position]['alt_discordant'+alt_index] = alt_discordant
				pileup[chrom][position]['SB'+alt_index] = SB

				# Allelic frequency = AF 
				AF = composition['VAF'+alt_index]
				# DePth = DP
				DP = composition['depth']
				# Allele Observations = AO
				AO = int(round(float(AF)*float(DP), 0))
				# HomoPolymer length = HP
				HP = composition['HP']
				# get error_base_probability
				base_error_probability = composition['base_error_probability']
				# get position mean qScore
				position_mean_qscore = composition['qScore']
				# get position standard deviation
				position_stdev = composition['qScore_stdev']
				# get qscore of the variant
				if alt_element == "del":
					variant_mean_qscore = "-"
				elif alt_element == "in":
					variant_mean_qscore = composition[alt_element]['qScore']
				else:
					variant_mean_qscore = composition[alt_element][3]

				# compute confidence level of the variant
				conf = ComputeConfidence(composition['q-value'+alt_index], alt_discordant, alt_concordant, HP, Cp, Cm, Vp, Vm, variant_mean_qscore, position_mean_qscore, position_stdev)
				CONF = conf[1]
				CONF_EXTRA = conf[1]+" ("+str(conf[0])+"/5)"



				# if the potential variant at this position passes 5 final filters => true variant
				# filter 1 : number of alt_concordant UMIs >= MIN_VARIANT_UMI value 
				# filter 2 : comptuted strand bias must be <= MAX_STRAND_BIAS value
				# filter 3 : homopolymer length must be <= MAX_HP_LENGTH value
				# filter 4 : total number of umis must be > to concordant UMIS / VAF
				# filter 5 : depth > MIN_VARIANT_DEPTH
				# if in white_list => ignore all filters


				if (AO >= MIN_VARIANT_DEPTH and SB <= MAX_STRAND_BIAS and HP <= MAX_HP_LENGTH and alt_concordant >= MIN_VARIANT_UMI and len(list(list(alt_umi)+list(noise_umi)))+ref_umi_len >= (float(composition['alt_concordant'+alt_index])/composition['VAF'+alt_index])) or (chrom+":"+str(position) in WHITE_LIST['pos']) or (chrom+":"+str(position-1) in WHITE_LIST['pos']):


					if chrom+":"+str(position) in WHITE_LIST['pos'] or chrom+":"+str(position-1) in WHITE_LIST['pos']:
						PASS_FILTER = "WHITE_LIST"
						IN_WHITE_LIST = True
					else:
						PASS_FILTER = "PASS"
						IN_WHITE_LIST = False

					# if the variant is not an indel
					if alt_element != "in" and alt_element != "del":

						# build index
						index = chrom+":"+str(position)
						# get its position
						positionInVCF = str(position)

						# build the variant name
						variantName = index+composition['ref']+">"+alt_element
						# give it the type SNV
						TYPE = 'SNV'

					# else if the variant is an insertion
					elif alt_element == "in":

						# build index (position in index is the position -1)
						index = chrom+":"+str(position-1)
						# gets its position (position in VCF is the position -1)
						positionInVCF = str(position-1)

						# choosing the most frequent insertion
						chosen_ins = ['', 0, 0]
						for ins, infos in pileup[chrom][position]['in'].items():
							if isinstance(infos, list):
								if (infos[0]+infos[1]) > (chosen_ins[1]+chosen_ins[2]):
									chosen_ins = [ins, infos[0], infos[1]]

						# build the variant name
						variantName = index+str(f[chrom][position-1-1])+">"+str(f[chrom][position-1-1])+chosen_ins[0]
						# give it the type INS
						TYPE = 'INS'

					# else if the variant is a deletion
					else:
						# build index (position in index is the position -1)
						index = chrom+":"+str(position-1)
						# gets its position (position in VCF is the position -1)
						positionInVCF = str(position-1)

						# choosing the most frequent deletion
						chosen_del = ['', 0, 0]
						for dele, infos in pileup[chrom][position]['del'].items():
							if (infos[0]+infos[1]) > (chosen_del[1]+chosen_del[2]):
								chosen_del = [dele, infos[0], infos[1]]


						# get the reference base from the reference dictionnary at the position-1
						lastBasePos = str(f[chrom][position-1-1]).upper()
						# get the deleted seq by going from position -> position+length 
						# of deleted sequence in the reference dictionnary
						dele = int(chosen_del[0])
						del_seq = ""
						while dele > 0:
							del_seq += str(f[chrom][position+len(del_seq)-1]).upper()
							dele -= 1

						# build the variant name
						variantName = index+lastBasePos+del_seq+">"+lastBasePos
						# giv it the type DEL
						TYPE = 'DEL'

					# build the REF and ALT columns of the VCF file for each 
					# variant type (SNV | INS | DEL)
					if TYPE == "SNV":
						REF = composition['ref']
						ALT = alt_element
					elif TYPE == "DEL":
						REF = lastBasePos+del_seq
						ALT = lastBasePos
					else:
						REF = str(f[chrom][position-1-1])
						ALT = str(f[chrom][position-1-1])+chosen_ins[0]


					# calculate umi-based allelic frequency
					UMI_AF = float(len(set(umi_list)))/(len(ref_umi)+len(noise_umi)+len(set(umi_list)))

				
					# create the INFO line | delimiter is ';'
					INFO = "AF="+str(AdaptiveRound(AF))+";UMI_AF="+str(AdaptiveRound(UMI_AF))+";AO="+str(AO)+";DP="+str(DP)+";HP="+str(HP)+";TYPE="+str(TYPE)+";CONF="+CONF.upper()
					


					# calculate total insertion observations on both strands
					n_ins_pos = 0
					n_ins_neg = 0
					for value in composition['in'].values():
						if isinstance(value, list):
							n_ins_pos += value[0]
							n_ins_neg += value[1]
					n_ins = n_ins_pos+n_ins_neg
					
					# calculate total deletion observations on both strands
					n_del_pos = 0
					n_del_neg = 0
					for value in composition['del'].values():
						n_del_pos += value[0]
						n_del_neg += value[1]
					n_del = n_del_pos+n_del_neg



					if (TYPE == "SNV" and variantName in WHITE_LIST['var']) or (TYPE == "DEL" and chrom+":"+positionInVCF+">del" in WHITE_LIST['var']) or (TYPE == "INS" and chrom+":"+positionInVCF+">ins" in WHITE_LIST['var']):
						WHITE_LIST_PASS = True
					else: 
						WHITE_LIST_PASS = False

					if (TYPE == "SNV" and variantName not in BLACK_LIST) or (TYPE == "DEL" and chrom+":"+positionInVCF+">del" not in BLACK_LIST ) or (TYPE == "INS" and chrom+":"+positionInVCF+">ins" not in BLACK_LIST):

						if (IN_WHITE_LIST == False) or (IN_WHITE_LIST == True and WHITE_LIST_PASS == True):

							# build the VCF line for each variant | delimiter = "\t"
							lineVCF = "\t".join([chrom, positionInVCF, variantName, REF, ALT, str(composition['q-value'+alt_index]), PASS_FILTER, INFO])
							

							# build the VARIANTS file line for each variant | delimiter = "\t"
							lineExtra = "\t".join([
								chrom, 
								positionInVCF, 
								"-", 
								REF, 
								index.replace(':', '-'), 
								str(composition['A'][0]+composition['A'][1]), 
								str(composition['C'][0]+composition['C'][1]), 
								str(composition['G'][0]+composition['G'][1]), 
								str(composition['T'][0]+composition['T'][1]), 
								"0", 
								"0", 
								str(n_ins), 
								str(n_del), 
								str(DP), 
								str((composition['A'][0]+composition['A'][1])/float(DP)),
								str((composition['C'][0]+composition['C'][1])/float(DP)),
								str((composition['G'][0]+composition['G'][1])/float(DP)),
								str((composition['T'][0]+composition['T'][1])/float(DP)),
								"0",
								str(n_ins/float(DP)),
								str(n_del/float(DP)),
								str((composition[composition['ref']][0]+composition[composition['ref']][1])/float(DP)),
								str(float(AO/float(DP))),
								"-", "-", "-", "-", "-", "-",
								"-", "-", "-", "-", "-", "-",
								"TRUE",
								alt_element,
								variantName.replace(':', ':g.'),
								"FALSE",
								str(ref_umi_len),
								str(len(umi_list)),
								str(len(ref_umi)), 
								str(len(set(umi_list))), 
								str(len(GetSingletons(total_ref_umi))),
								str(n_singleton_UMI),
								str(n_monocluster_UMI),
								str(n_polycluster_UMI),
								str(alt_discordant),
								str(alt_concordant),
								str(variant_mean_qscore),
								str(position_mean_qscore),
								str(position_stdev),
								str(base_error_probability),
								str(composition['p-value'+alt_index]),
								str(composition['q-value'+alt_index]),
								TYPE,
								str(SB),
								str(HP),
								CONF_EXTRA,
								str(pos_covAllele),
								str(neg_covAllele),
								str(pos_covTotal),
								str(neg_covTotal),
								str(UMI_AF)
								])
							
							# add both lines to the finalvariants dictionnary
							# add infos to the phased variants dictionnary
							finalVariants[chrom+"|v"+alt_index][position] = [lineVCF, lineExtra]
							phasedVariants[chrom+"|v"+alt_index][position] = {'line' : lineVCF, 'umi': alt_concordant_umi}

			# increment to track progress
			currentPos += 1






	finalVariants = CorrectDeletions(finalVariants)
	phasedVariants = CorrectDeletions(phasedVariants)




	print("\n")
	PrintTime('console', "\tDone")

	# return finalVariants dictionnary
	return [finalVariants, phasedVariants]




def ParseFASTA(filePath):
	f = {}
	chrom = ""

	for line in open(filePath):
		line = line.strip()
		# if FASTA file header contains spaces, split and only consider first item
		line = line.split(" ")[0]
		if '>' in line:
			if chrom in f.keys():
				f[chrom] = "".join(list(f[chrom]))
			chrom = line[1:]
			f[chrom] = []
		else:
			f[chrom].append(line)

	return f



# a function that allows to detect spaces in file paths 
# and return an error
def DetectSpaces(argList):
	i=0
	iList = []
	for arg in argList:
		if arg.startswith('-'):
			iList.append(i)
		if arg == argList[-1]:
			iList.append(i+1)
		i += 1	

	for i in range(0, len(iList)-1):
		j = i+1
		if iList[j] - iList[i] > 2:
			return argList[iList[i]]+"="+" ".join(argList[iList[i]+1:iList[j]])
	
	return False




# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY DELETED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : 
# 		  -BED               : (STR)  THE PATH OF THE BED FILE
#
# VALUE : -PILEUP            : (DICT) A DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		



def ParseBED(bed):	

	# create an empty dictionnary for pileup counters
	pileup = {}

	# read the bed file line by line
	for line in open(bed):

		# remove trailing white spaces in line
		line = line.strip()
		
		# split the line using tab as delimiter
		line = line.split('\t')

		if len(line) >= 3:
			# first element is the chromosome
			chrom = line[0]

			# second element is the start of the region
			# third element is the end of the region
			# create a list containing start and end 
			# sort to ensure that start < end
			limits = [int(line[1])+1, int(line[2])]
			limits.sort()

			# for each position in the [start;end] interval
			for pos in range(limits[0], limits[1]+1):
				
				# try to add the position to the pileup for the specified chromosome
				# if the chromosome was already aded to the pileup
				try:
					# add the position to the chrom with the default counters
					pileup[chrom][pos] = { 'A': [0, 0, [], 0, []], 'C': [0, 0, [], 0, []], 'G': [0, 0, [], 0, []], 'T': [0, 0, [], 0, []], 'in': {'qScore': 0}, 'del': {}, 'base_error_probability': [] } 
				except:
					# add the chrom to the pileup with the first position and the default counters
					pileup[chrom] = { pos: { 'A': [0, 0, [], 0, []], 'C': [0, 0, [], 0, []], 'G': [0, 0, [], 0, []], 'T': [0, 0, [], 0, []], 'in': {'qScore': 0}, 'del': {}, 'base_error_probability': [] } }



	# the pileup dictionnary contains :
	# for each chromosome:
	# 		for each position:
	#		{
	#			'A'   : 0 | 0 | list() | 0
	#			'C'   : 0 | 0 | list() | 0
	#			'T'   : 0 | 0 | list() | 0
	#			'G'   : 0 | 0 | list() | 0
	#			'in'  :     {}
	#			'del' :     {}
	#			'base_error_probability' : 0
	#		}
	# for the four normal bases, this structure 0 | 0 | set() | 0 is a list containing 
	# a counter for the forward strand, a counter for the reverse strand, a set
	# that will contain the UMIs of the reads and the average qscore per base.
	# a set was used instead of a list to contain unique UMIs only  
	#
	# the insertion dictionnary will be updated each time an insertion is encountered
	# and will end up looking like this:
	# {
	# 	'inserted_seq_1' : N1_+ | N1_- | list()
	# 	'inserted_seq_2' : N2_+ | N2_- | list()
	# 	'inserted_seq_n' : Nn_+ | Nn_- | list(),
	#	'qScore' : 0
	# }
	#
	# the deletion dictionnary will be updated each time a deletion is encountered
	# and will end up looking like this:
	# {
	# 	len of the deleted segment_1 : N1_+ | N1_- | list()
	# 	len of the deleted segment_2 : N2_+ | N2_- | list()
	# 	len of the deleted segment_n : Nn_+ | Nn_- | list()
	# }	
	# 
	# the base_error_probability by bases qscores at each position
	# at the end, this value will be divided by depth to get the mean qscore a the location
	# the the mean qscore will be used to estimate the noise at the location

	# sort pileup
	pileup = SortPileup(pileup)
	
	# return the pileup
	return pileup














def Call(config, FUNC_PATH):

	# load and define variables from the config
	INPUT                 = config['input']
	BED                   = config['bed']
	FASTA                 = config['fasta']
	MIN_BASE_QUALITY      = int(config['min_base_quality'])
	MIN_MAPPING_QUALITY   = int(config['min_mapping_quality'])
	MIN_READ_QUALITY      = int(config['min_read_quality'])
	MIN_VARIANT_UMI       = int(config['min_variant_umi'])
	MIN_VARIANT_DEPTH     = int(config['min_variant_depth'])
	STRAND_BIAS_METHOD    = str(config['strand_bias_method'])
	MAX_STRAND_BIAS       = float(config['max_strand_bias'])
	PILEUP                = config['pileup']
	REBUILD               = False if os.path.isfile(PILEUP) else True
	OUTPUT                 = config['output']
	CORES                  = config['cores']
	DEFAULT_CORES          = config['default_cores']
	ALPHA                  = float(config['alpha'])
	MAX_HP_LENGTH          = int(config['max_hp_length'])
	gVCF                   = config['gvcf']
	KEEP_PILEUP			   = config['keep_pileup']
	BLACK_LIST             = config['black_list']
	WHITE_LIST             = config['white_list']
	MIN_PHASE_UMI          = int(config['min_phase_umi'])
	MIN_PHASE_VAF_RATIO    = float(config['min_phase_vaf_ratio'])
	MAX_PHASE_DISTANCE     = int(config['max_phase_distance'])
	IGNORE_MONOCLUSTER_UMI = config['ignore_monocluster_umi']
	COMPUTE_COVERAGE_STATS = config['compute_coverage_stats']
	VERSION                = config['version']
	LAST_UPDATE            = config['lastUpdate']


	# print parameters in the console
	PrintTime("green", "\t\tINPUT file     : "+INPUT)
	PrintTime("green", "\t\tBED file       : "+BED)
	PrintTime("green", "\t\tFASTA file     : "+FASTA)

	if PILEUP != "None":	
		PrintTime("green", "\t\tPILEUP file    : "+PILEUP)
	PrintTime("green", "\t\tOutput         : "+OUTPUT)

	PrintTime("green", "\t\tmin_base_quality       : "+str(MIN_BASE_QUALITY))
	PrintTime("green", "\t\tmin_read_quality       : "+str(MIN_READ_QUALITY))
	PrintTime("green", "\t\tmin_mapping_quality    : "+str(MIN_MAPPING_QUALITY))
	PrintTime("green", "\t\tmin_variant_umi        : "+str(MIN_VARIANT_UMI))
	PrintTime("green", "\t\tmin_variant_depth      : "+str(MIN_VARIANT_DEPTH))
	PrintTime("green", "\t\tstrand_bias_method     : "+str(STRAND_BIAS_METHOD))
	PrintTime("green", "\t\tmax_strand_bias        : "+str(MAX_STRAND_BIAS))
	PrintTime("green", "\t\tmax_hp_length          : "+str(MAX_HP_LENGTH))
	PrintTime("green", "\t\talpha                  : "+str(ALPHA))
	PrintTime("green", "\t\tmin_phase_umi          : "+str(MIN_PHASE_UMI))
	PrintTime("green", "\t\tmin_phase_vaf_ratio    : "+str(MIN_PHASE_VAF_RATIO))
	PrintTime("green", "\t\tmax_phase_distance     : "+str(MAX_PHASE_DISTANCE))
	if gVCF:
		PrintTime("green", "\t\tgVCF                   : "+str(gVCF)+" (Experimental)")
	else:
		PrintTime("green", "\t\tgVCF                   : "+str(gVCF))

	if DEFAULT_CORES:
		PrintTime("green", "\t\tcores                  : "+str(CORES)+" (default)")
	else:
		PrintTime("green", "\t\tcores                  : "+str(CORES))

	PrintTime("green", "\t\tkeep_pileup            : "+str(KEEP_PILEUP))
	PrintTime("green", "\t\tignore_monocluster_umi : "+str(IGNORE_MONOCLUSTER_UMI))
	PrintTime("green", "\t\tcompute_coverage_stats : "+str(COMPUTE_COVERAGE_STATS))
	PrintTime("green", "\t\tblack_list             : "+str(BLACK_LIST))
	PrintTime("green", "\t\twhite_list             : "+str(WHITE_LIST)+"\n")

	PrintTime("green", "\t\tVERSION                : "+VERSION)
	
	PrintTime("console", "\tDone\n")



	# make dir if outdir doesn't exist
	try:
		os.mkdir(OUTPUT)
	except:
		pass 



	# load the reference genome file
	f = ParseFASTA(FASTA)




	# if input is bam => launch samtools view command
	# to convert it to sam
	if ".bam" in INPUT and ".sam" not in INPUT:

		print("\n")
		PrintTime('console', "\tConverting BAM to SAM...")

		SAM = BAMtoSAM(INPUT)

		PrintTime('console', "\tDone")

	else:
		# else => sam = input
		SAM = INPUT



	nReads = GetTotalLines(SAM)
	validReads = 0



	# if a pileup is not given, the pileup has to be build
	if REBUILD:


		print('\n')
		PrintTime('console', '\tAnalyzing BED...')

		# build the empty pileup
		pileup = ParseBED(BED)
		

		pileupLen = GetPileupLength(pileup)
		# print(pileupLen); exit()

		if CORES > 1:

			# if pileup length < 1M positions => using multiple cores is advantageous
			# if 1M < pileup length <= 2M => a minimum of 5M reads is required to see performance gains 
			# if 2M < pileup length <= 5M => a minimum of 10M reads is required to see performance gains 
			# if 5M < pileup length <= 10M => a minimum of 50M reads is required to see any performance gains 
			# if pileup length > 10M, theoretically, 100M reads should be analyzed faster but the memory usage will skyrocket so automatically switch to 1 core only.
			if ((pileupLen > 1000000 and pileupLen <= 2000000 and nReads < 5000000) or (pileupLen > 2000000 and pileupLen <= 5000000 and nReads < 20000000) or (pileupLen > 5000000 and pileupLen <= 10000000 and nReads < 50000000) or (pileupLen > 10000000)):

				PrintTime('warning', "\t\tWarning: Using more cores to analyze the provided data will not result in any significant performance gains!\n\t\t\t\tLaunching UMI-VarCal on one core only...\n")
				CORES = 1




		# dump the pileup object
		with open(OUTPUT+"/.pileup_ini", 'wb') as handle:
			msgpack.pack(pileup, handle, encoding="utf-8")
			# pickle.dump(pileup, handle, protocol=pickle.HIGHEST_PROTOCOL)

		

		PrintTime('console', '\tDone\n')


		if KEEP_PILEUP:
			PrintTime('warning', "\tWarning: keep_pileup parameter is set to True. This can affect your memory consumption.\n\t\t\tIf your system runs out of memory, please try to run the analysis with --keep_pileup False instead.\n")
		PrintTime('console', "\tBuilding Pileup...")
	 	

		# if multiple cores are used
		# wait until all processes are finished == until all files are generated
		if CORES > 1:

			#############################################################################################
			###########################                                       ###########################
			########################### PARALLELIZED CODE START : TREAT READS ###########################
			###########################                                       ###########################
			#############################################################################################

			# split the SAM files into equal sub files
			nReads_split = int(nReads/float(CORES))

			# preprocess reads
			# if more then one core is to be used, separate the input into subfiles
			subFiles = PreprocessReads(SAM, CORES)


			# parallelization block
			p=Pool(processes=int(CORES))
			tmp = []
			for subFile in subFiles:
				# get the values from each core and add return values to the tmp array
				tmp.append(p.apply_async(TreatReads, (subFile, GetTotalLines(subFile), OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY)))
			p.close()
			p.join()

			# parse the tmp array and get pileups and valid reads from each core
			pileups = []
			subValidReads = []
			for x in tmp:
				pileups.append(x.get()[0])
				subValidReads.append(x.get()[1])


			# total valid reads = the sum of valid reads from each core
			validReads = sum(subValidReads)


			# merge sub pileups to obtain whole pileup
			pileup = MergeSubPileups(pileup, pileups, subFiles, OUTPUT)

			#########################################################################################
			###########################                                     #########################
			########################### PARALLELIZED CODE END : TREAT READS #########################
			###########################                                     #########################
			#########################################################################################

		else:

			# if only one core is to used, launch the function from here since no need to merge
			value = TreatReads(SAM, nReads, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY)
			pileup = value[0]
			validReads = value[1]


		print("\n")
		PrintTime('console', "\tDone")



		print("\n")
		PrintTime('console', "\tEstimating Noise in Reads...")
		
		

		# add depth to pileup
		# add variant error noise at each position
		# add reference bases in the dictionnary
		# add homopolymers infos

		pileup = Add_Depth_Noise_Ref_HP(pileup, f)


		# rebuild to SAM original name
		SAM = SAM.replace('_reordered.sam', ".sam")

		# dump pileup in msgpack object
		if KEEP_PILEUP:
			with open(OUTPUT+"/"+SAM.replace(".sam", ".pileup").split("/")[-1], 'wb') as handle:
				msgpack.pack(pileup, handle, encoding="utf-8")



		# remove tmp empty pileup file
		try:
			os.remove(OUTPUT+"/.pileup_ini")
		except:
			pass


		print("\n")
		PrintTime('console', "\tDone")



	else:

		print("\n")
		PrintTime('console', "\tLoading Pileup...")
		
		# load pileup from msgpack object
		with open(PILEUP, 'rb') as handle:
			pileup = msgpack.unpack(handle, encoding="utf-8", max_buffer_size=100*1024*1024*1024)
			pileup = SortPileup(pileup)

			
		PrintTime('console', "\tDone")



	### Remove all SAM files
	os.remove(SAM)
	if CORES > 1:
		for subFile in subFiles:
			os.remove(subFile)




	### Compute UMI coverage statistics per region & position 
	if COMPUTE_COVERAGE_STATS:
		ComputeCoverageStats(pileup, BED, OUTPUT, SAM)


	pileupLen = GetPileupLength(pileup)

	# print(pileup)
	full_pileup = CopyPileup(pileup)






	### parse black and white lists if provided
	if BLACK_LIST != "None" or WHITE_LIST != "None":
		print("\n")
		PrintTime('console', "\tParsing list(s)...")
		
		value = ParseLists(BLACK_LIST, WHITE_LIST)
		BLACK_LIST = value[0]
		WHITE_LIST = value[1]

		PrintTime('console', "\tDone")
	else:
		BLACK_LIST = []
		WHITE_LIST = {'pos': [], 'var': []} 





	### Poisson modeling to filter positions
	result = FilterPositions(pileup, ALPHA, WHITE_LIST)
	pileup = result[0]
	potential = result[1]
	foundCandidates = result[2]

	if foundCandidates:

		### call final variants
		value = CallVariants(pileup, f, STRAND_BIAS_METHOD, MAX_STRAND_BIAS, MIN_VARIANT_UMI, MIN_VARIANT_DEPTH, MAX_HP_LENGTH, BLACK_LIST, WHITE_LIST, IGNORE_MONOCLUSTER_UMI)
		finalVariants = value[0]
		phasedVariants = value[1]

		### call phased variants
		phasedVariants = Phase(phasedVariants, MIN_PHASE_UMI, MIN_PHASE_VAF_RATIO, MAX_PHASE_DISTANCE)

		### Writing results to VCF
		final = Output(full_pileup, pileup, finalVariants, phasedVariants, INPUT, SAM, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, STRAND_BIAS_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, gVCF, VERSION)

		# calculate and display stats
		CalculateStats(pileup, potential, final, pileupLen, nReads, validReads, REBUILD)
	else:
		print("\n")
		message = "No candidate positions were found !\n"
		PrintTime('error', "\t"+message)

		### Writing results to VCF (even if no variants found)
		finalVariants = {}
		phasedVariants = {}
		final = Output(full_pileup, pileup, finalVariants, phasedVariants, INPUT, SAM, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, STRAND_BIAS_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, gVCF, VERSION)

		print('\r')
		PrintTime('console', "\tCalculating statistics...")
		
		# print out stats to console
		message = "Candidate Positions: 0"
		PrintTime('green', "\t\t"+message)
		message = "Final Variants: 0"
		PrintTime('green', "\t\t"+message)
		









def GenerateVCFHeader(INPUT, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, SB_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, VERSION):
	
	date = datetime.datetime.now().strftime("%Y-%m-%d")

	l1 = '##fileformat=VCFv4.2\n'
	l2 = '##fileDate="'+date+'"\n'
	l3 = '##source="UMI-VarCal"\n'
	l4 = '##toolVersion="'+VERSION+'"\n'
	l5 = '##command="input=\''+INPUT+'\', bed=\''+BED+'\', fasta=\''+FASTA+'\', out=\''+OUTPUT+'\', min_base_quality='+str(MIN_BASE_QUALITY)+', min_read_quality='+str(MIN_READ_QUALITY)+', min_mapping_quality='+str(MIN_MAPPING_QUALITY)+', min_variant_umi='+str(MIN_VARIANT_UMI)+', strand_bias_method=\''+str(SB_METHOD)+'\', max_strand_bias='+str(MAX_STRAND_BIAS)+', alpha='+str(ALPHA)+', max_hp_length='+str(MAX_HP_LENGTH)+', cores='+str(CORES)+'\"\n'
	l6 = '##reference="'+FASTA+'"\n'
	rest = """##FILTER=<ID=ID,Description="Generic filter">
##INFO=<ID=AF,Number=1,Type=Float,Description="Allele frequency based on allele count/depth">
##INFO=<ID=UMI_AF,Number=2,Type=Float,Description="Allele frequency based on UMI count/depth">
##INFO=<ID=AO,Number=3,Type=Integer,Description="Alternate allele observation count">
##INFO=<ID=DP,Number=4,Type=Integer,Description="Total read depth at the locus">
##INFO=<ID=HP,Number=5,Type=Integer,Description="Homopolymer length at the locus">
##INFO=<ID=TYPE,Number=6,Type=String,Description="The variation type: either SNV, INS or DEL">
##INFO=<ID=CONF,Number=7,Type=String,Description="Confidence of the variant. It has 5 levels: low(1/5), average(2/5), high(3/5), strong(4/5) or certain(5)">
##INFO=<ID=PHASED,Number=8,Type=List(String),Description="List of phased variants">
##INFO=<ID=PHASED_UMI,Number=9,Type=List(Integer),Description="List of the number of common UMI between variants. Each number represents the number of common UMI between the main variant and the specified phased variant">
##INFO=<ID=PHASED_CUMULATIVE_UMI,Number=10,Type=List(Integer),Description="List of the number of cumulative common UMI between variants. Each number represents the number of common UMI between the main variant, the specified phased variant and all the phased variants occuring before it">
##INFO=<ID=PHASED_VAF_RATIO,Number=11,Type=List(Float),Description="List of the VAF Ratio  with every phased variant">
#CHROM	POS	ID	REF	ALT	Q-VALUE	FILTER	INFO
"""

	return l1+l2+l3+l4+l5+l6+rest



# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY DELETED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -READNAME          : (STR)  NAME/ID OF THE READ
# 		  -UMI               : (STR)  UMI SEQUENCE OF THE READ
# 		  -CHROM             : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -START             : (INT)  THE START POSITION OF THE READ
# 		  -SEQ               : (STR)  THE SEQUENCE OF THE READ
# 		  -STRAND            : (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CURSOR_POS        : (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ        : (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
# 		  -MAXX              : (INT)  THE LENGTH OF THE CIGAR 'D' ELEMENT
#
# VALUE : -POSITION          : (INT)  THE FINAL POSITION THAT'S BEEN PARSED IN THE READ
#		  -CURSOR_POS        : (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ        : (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
#

def AddDeletions(pileup, readName, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx):

	# create a cursor to move in the deleted sequence 
	del_cursor = 0

	# for each position between limits
	for position in range(start+cursor_pos, start+cursor_pos+maxx):
		
		# try because base position in read could not be in BED file
		# therefore, the position could not be in the PILEUP dictionnary
		# if position not in BED, skip the read
		try:
			# test if chrom and position are in the PILEUP dictionnary
			test = pileup[chrom][position]
			
			# if this deletion was already seen at this location
			# maxx-del_cursor corresponds to the length of the deleted sequence
			# this number will serve as an entry in the PILEUP dictionnary for
			# deleted sequences
			try:
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position]['del'][maxx-del_cursor][strand] += 1
				# add the umi to the corresponding set specific to the deleted sequence
				pileup[chrom][position]['del'][maxx-del_cursor][2].append(umi)
				# add the readName to the corresponding list specific to the base in the PILEUP dictionnary
				pileup[chrom][position]['del'][maxx-del_cursor][3].append(readName)

			# if this is the first time we see this deleted sequence at this position, 
			# an entry should be created with its appropriated structure (2 counters + set)  
			except:
				# create the entry in PILEUP dictionnary
				pileup[chrom][position]['del'][maxx-del_cursor] = [0, 0, [umi], [readName]]
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position]['del'][maxx-del_cursor][strand] += 1


			
			# advance in the deleted sequence if the deletion length > 1
			del_cursor += 1


		# if position not in BED, skip the read
		except:
			pass	


	# increment the cursor_pos with the length of the cigar element when done adding it
	cursor_pos += maxx

	# return position, cursor_seq and cursor_pos to continue from where we left off
	return [position, cursor_seq, cursor_pos]







def Output(full_pileup, pileup, finalVariants, phasedVariants, INPUT, SAM, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, SB_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, gVCF, VERSION):

	print("\n")
	PrintTime('console', "\tGenarating VCF & gVCF...") if gVCF else PrintTime('console', "\tGenarating VCF...")

	vcf = open(OUTPUT+"/"+SAM.replace(".sam", ".vcf").split("/")[-1], "w")
	vcf.write(GenerateVCFHeader(INPUT, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, SB_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, VERSION))

	if gVCF:
		gvcf = open(OUTPUT+"/"+SAM.replace(".sam", ".gvcf").split("/")[-1], "w")
		gvcf.write(GenerateGVCFHeader(INPUT, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, SB_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, VERSION))

	variantsFile = open(OUTPUT+"/"+SAM.replace(".sam", ".variants").split("/")[-1], "w")
	header='\t'.join(map(str,["chr","pos","amplicon","reference","idx","A","C","G","T","N","=","+","-","depth","fA","fC","fG","fT","f=","f+","f-","fref","falt","pFisherA","pFisherT","pFisherG","pFisherC","pFisherIns","pFisherDel","pFisherA_adjust","pFisherT_adjust","pFisherG_adjust","pFisherC_adjust","pFisherIns_adjust","pFisherDel_adjust","variantCall","alternative","GR","parsed","n_UMI_wt","n_UMI_mt","n_unique_UMI_wt","n_unique_UMI_mt","n_singleton_UMI_wt","n_singleton_UMI_mt","n_monocluster_UMI_mt","n_polycluster_UMI_mt","n_discordant_UMI_mut","n_concordant_UMI_mut", "mean_variant_qScore", "mean_position_qScore", "position_stdev", "base_error_probability", "p-value","q-value","type","SB","HP","confidence","allele_cov_plus", "allele_cov_minus", "coverage_plus", "coverage_minus", "umi_allelic_freq", "phased", "phased_common_umi", "phased_cumulative_common_umi", "phase_vaf_ratio"]))
	variantsFile.write(header+"\n")

	currentPos = 1.0
	lastProgress = 0.0
	totalPos = GetPileupLength(finalVariants)

	pos_to_skip = []

	# counter of n sub, del and ins in VCF
	kept = [0, 0, 0]


	variants = {}
	
	for chrom in finalVariants.keys():

		for position, info in finalVariants[chrom].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)



			try:
				phasedinfos = phasedVariants[info[0].split("\t")[2]]
				phased_var_list = list(phasedinfos.keys())
				phased_com_umi = []
				phased_cumulative_com_umi = []
				phased_vaf_ratio = []

				
				for x in phased_var_list:
					phased_com_umi.append(phasedinfos[x]['com_umi'])
					phased_cumulative_com_umi.append(phasedinfos[x]['cumulative_com_umi'])
					phased_vaf_ratio.append(phasedinfos[x]['vaf_ratio'])


				phased_var_list = ";".join(phased_var_list)
				phased_com_umi = ";".join(phased_com_umi)
				phased_cumulative_com_umi = ";".join(phased_cumulative_com_umi)
				phased_vaf_ratio = ";".join(phased_vaf_ratio)



			except:
				phased_var_list = ""
				phased_com_umi = ""
				phased_vaf_ratio = ""
				phased_cumulative_com_umi = ""


			if len(phased_var_list) == 0:
				vcf.write(info[0]+";PHASED=NA;PHASED_UMI=NA;PHASED_CUMULATIVE_UMI=NA;PHASED_VAF_RATIO=NA;\n")
			else:	
				vcf.write(info[0]+";PHASED="+phased_var_list.replace(";", ",")+";PHASED_UMI="+phased_com_umi.replace(";", ",")+";PHASED_CUMULATIVE_UMI="+phased_cumulative_com_umi.replace(";", ",")+";PHASED_VAF_RATIO="+phased_vaf_ratio.replace(";", ",")+"\n")
			
			variantsFile.write(info[1]+"\t"+phased_var_list+"\t"+phased_com_umi+"\t"+phased_cumulative_com_umi+"\t"+phased_vaf_ratio+"\n")
			
			if ";TYPE=SNV;" in info[0]:
				kept[0] += 1
			elif ";TYPE=DEL;" in info[0]:
				kept[1] += 1
			elif ";TYPE=INS;" in info[0]:
				kept[2] += 1

			variants[chrom.split("|")[0]+"|"+str(position)] = chrom.split("|")[1]


			currentPos += 1


	vcf.close()
	variantsFile.close()





	if gVCF:
		# creating gVCF
		gVCF = OrderedDict()
		for chrom in full_pileup.keys():
			gVCF[chrom] = OrderedDict()
			block_counter = 1
			var_counter = 1
			del_positions = []


			notVariants = []
			var = []

			for position in full_pileup[chrom].keys():

				if chrom+"|"+str(position) not in variants.keys():
					if position not in var:
						notVariants.append(position)
				else:
					var_type = finalVariants[chrom+"|"+variants[chrom+"|"+str(position)]][position][0].split("\t")[7].split(";")[5].split("=")[1]
					if var_type == "SNV":
						var.append(position)
					elif var_type == "DEL":
						if position not in var:
							del_len = len(finalVariants[chrom+"|"+variants[chrom+"|"+str(position)]][position][0].split("\t")[3])
							for i in range(position-1, position+del_len-1):
								var.append(i)

							try:
								notVariants.remove(position-1)
							except:
								pass

					elif var_type == "INS":
						var.append(position-1)
						var.append(position)

						try:
							notVariants.remove(position-1)
						except:
							pass



			for x in notVariants:
				if 'block-'+str(block_counter) not in gVCF[chrom].keys():
					gVCF[chrom]['block-'+str(block_counter)] = [x]
				else:
					if len(gVCF[chrom]['block-'+str(block_counter)]) == 1:
						gVCF[chrom]['block-'+str(block_counter)].append(x)
					else:
						if x == gVCF[chrom]['block-'+str(block_counter)][-1]+1:
							gVCF[chrom]['block-'+str(block_counter)].append(x)
						else:
							last_x = gVCF[chrom]['block-'+str(block_counter)][-1] 
							
							if last_x+1 in var:

								gVCF[chrom]['variant-'+str(var_counter)] = [last_x+1]
								var_counter += 1


							block_counter += 1
							gVCF[chrom]['block-'+str(block_counter)] = [x]



		for chrom in gVCF.keys():
			print(gVCF[chrom].keys())			

		for chrom in gVCF.keys():
			for block, positions in gVCF[chrom].items():
				if 'block' in block:
					start = positions[0]
					end = positions[-1]
					qScores = []
					depths = []

					for pos in range(start, end+1):
						qScores.append(full_pileup[chrom][pos]['qScore'])
						depths.append(full_pileup[chrom][pos]['depth'])

					qScore = int(round(sum(qScores)/float(len(qScores)), 1))
					depth  = round(sum(depths) /float(len(depths)), 0)

					line = chrom+"\t"+str(start)+"\t"+"-"+"\t"+full_pileup[chrom][start]['ref']+"\t"+"<NON_REF>"+"\t"+"-"+"\t"+"-"+"\t"+"END="+str(end)+";"+"MEAN_DP="+str(depth)+";"+"MEAN_QSCORE="+str(qScore)+"\n"
					
				
				else:
					position = positions[0]
					try:
						line = finalVariants[chrom+"|"+variants[chrom+"|"+str(position)]][position][0]
					except:
						line = finalVariants[chrom+"|"+variants[chrom+"|"+str(position+1)]][position+1][0]
					
					line = line.split("\t")
					line[4] += ",<NON_REF>"
					line = "\t".join(line)+"\n"

				gvcf.write(line)


		gvcf.close()

	print("\n")
	PrintTime('console', "\tDone")



	return kept


# THIS FUNCTION ALLOWS TO MERGE MULTIPLE SUBPILEUPS TO CREATE ONE WHOLE PILEUP.
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#       : -PILEUPS           : (LIST) A LIST CONTAINING ALL THE SUBPILEUPS TO BE MERGED
#       : -SUBFILES          : (LIST) THE LIST OF SUBFILES NAMES
#
# VALUE : -POSITION          : (INT)  THE FINAL POSITION THAT'S BEEN PARSED IN THE READ
#		  -CURSOR_POS        : (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ        : (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
#	



def MergeSubPileups(pileup, pileups, subFiles, OUTPUT):


	# check that the number of subfiles == the number of given cub pileups
	# the 2 number must be equal. If not, an error occured in the creation
	# of some pileups ==> fatal error ==> script exists with error
	if len(pileups) != len(subFiles):
		print('\n')
		PrintTime("error", "\tError while attempting to merge pileups : lengths differ!\n\t\t\tExiting...")

	# if the pileup lists == 1 ==> no merging has to be done ==> return the pileup directly
	if len(pileups) == 1:
		return pileups[0]



	# for each chromosome
	for chrom in pileup.keys():

		# for each position and its counters
		for position, composition in pileup[chrom].items():

			# loop through the sub pileups list
			for p in pileups:
				
				# increment the A forward counters
				pileup[chrom][position]['A'][0] += p[chrom][position]['A'][0]
				# increment the A reverse counters
				pileup[chrom][position]['A'][1] += p[chrom][position]['A'][1]
				# add the umis to the A unique umis set 
				pileup[chrom][position]['A'][2] += p[chrom][position]['A'][2]
				# add the qScores to the A qScores list 
				pileup[chrom][position]['A'][3] += p[chrom][position]['A'][3]
				# add the readNames to the corresponding base list 
				pileup[chrom][position]['A'][4] += p[chrom][position]['A'][4]


				# increment the A forward counters
				pileup[chrom][position]['C'][0] += p[chrom][position]['C'][0]
				# increment the C reverse counters
				pileup[chrom][position]['C'][1] += p[chrom][position]['C'][1]
				# add the umis to the C unique umis set	
				pileup[chrom][position]['C'][2] += p[chrom][position]['C'][2]
				# add the qScores to the C qScores list 
				pileup[chrom][position]['C'][3] += p[chrom][position]['C'][3]
				# add the readNames to the corresponding base list 
				pileup[chrom][position]['C'][4] += p[chrom][position]['C'][4]


				# increment the G forward counters
				pileup[chrom][position]['G'][0] += p[chrom][position]['G'][0]
				# increment the G reverse counters
				pileup[chrom][position]['G'][1] += p[chrom][position]['G'][1]
				# add the umis to the G unique umis set	
				pileup[chrom][position]['G'][2] += p[chrom][position]['G'][2]
				# add the qScores to the G qScores list 
				pileup[chrom][position]['G'][3] += p[chrom][position]['G'][3]
				# add the readNames to the corresponding base list 
				pileup[chrom][position]['G'][4] += p[chrom][position]['G'][4]


				# increment the T forward counters
				pileup[chrom][position]['T'][0] += p[chrom][position]['T'][0]
				# increment the T reverse counters
				pileup[chrom][position]['T'][1] += p[chrom][position]['T'][1]
				# add the umis to the T unique umis set	
				pileup[chrom][position]['T'][2] += p[chrom][position]['T'][2]
				# add the qScores to the T qScores list 
				pileup[chrom][position]['T'][3] += p[chrom][position]['T'][3]
				# add the readNames to the corresponding base list 
				pileup[chrom][position]['T'][4] += p[chrom][position]['T'][4]


				# add the insertions on the sub pileup to the big pileup
				# for each insertion in the insertion dictionnary
				for ins in p[chrom][position]['in'].keys():
					if ins != "qScore":
						# try-except block
						try:
							# if the insertion is already present ==> increment this insertion counters
							pileup[chrom][position]['in'][ins][0] += p[chrom][position]['in'][ins][0]
							pileup[chrom][position]['in'][ins][1] += p[chrom][position]['in'][ins][1]
							pileup[chrom][position]['in'][ins][2] += p[chrom][position]['in'][ins][2]
							pileup[chrom][position]['in'][ins][3] += p[chrom][position]['in'][ins][3]
							pileup[chrom][position]['in']['qScore'] += p[chrom][position]['in']['qScore']
						except:
							# if insertion is not present in the insertions dict ==> it has to be inserted
							# with its own counters and unique umis set
							pileup[chrom][position]['in'][ins] = [p[chrom][position]['in'][ins][0], p[chrom][position]['in'][ins][1], p[chrom][position]['in'][ins][2], p[chrom][position]['in'][ins][3]]
							pileup[chrom][position]['in']['qScore'] = p[chrom][position]['in']['qScore']



				# add the deletions on the sub pileup to the big pileup
				# for each deletion in the deletion dictionnary
				for dele in p[chrom][position]['del'].keys():
					# try-except block
					try:
						# if the deletion is already present ==> increment this deletion counters
						pileup[chrom][position]['del'][dele][0] += p[chrom][position]['del'][dele][0]
						pileup[chrom][position]['del'][dele][1] += p[chrom][position]['del'][dele][1]
						pileup[chrom][position]['del'][dele][2] += p[chrom][position]['del'][dele][2]
						pileup[chrom][position]['del'][dele][3] += p[chrom][position]['del'][dele][3]
					except:
						# if deletion is not present in the deletions dict ==> it has to be inserted
						# with its own counters and unique umis set
						pileup[chrom][position]['del'][dele] = [p[chrom][position]['del'][dele][0], p[chrom][position]['del'][dele][1], p[chrom][position]['del'][dele][2], p[chrom][position]['del'][dele][3]]


				# increment the total base quality scores 
				pileup[chrom][position]['base_error_probability'] += p[chrom][position]['base_error_probability']
	
	# return the final whole pileup
	return pileup


	



def GenerateGVCFHeader(INPUT, BED, FASTA, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, MIN_VARIANT_UMI, SB_METHOD, MAX_STRAND_BIAS, CORES, ALPHA, MAX_HP_LENGTH, VERSION):
	
	date = datetime.datetime.now().strftime("%Y-%m-%d")

	l1 = '##fileformat=GVCFv4.2\n'
	l2 = '##fileDate="'+date+'"\n'
	l3 = '##source="UMI-VarCal"\n'
	l4 = '##toolVersion="'+VERSION+'"\n'
	l5 = '##command="input=\''+INPUT+'\', bed=\''+BED+'\', fasta=\''+FASTA+'\', out=\''+OUTPUT+'\', min_base_quality='+str(MIN_BASE_QUALITY)+', min_read_quality='+str(MIN_READ_QUALITY)+', min_mapping_quality='+str(MIN_MAPPING_QUALITY)+', min_variant_umi='+str(MIN_VARIANT_UMI)+', strand_bias_method=\''+str(SB_METHOD)+'\', max_strand_bias='+str(MAX_STRAND_BIAS)+', alpha='+str(ALPHA)+', max_hp_length='+str(MAX_HP_LENGTH)+', cores='+str(CORES)+'\"\n'
	l6 = '##reference="'+FASTA+'"\n'
	rest = """##ALT=<ID=NON_REF,Description="Represents any possible alternative allele at this location">
##FILTER=<ID=ID,Description="Generic filter">
##INFO=<ID=AF,Number=1,Type=Float,Description="Allele frequency based on allele count/depth">
##INFO=<ID=UMI_AF,Number=2,Type=Float,Description="Allele frequency based on UMI count/depth">
##INFO=<ID=AO,Number=3,Type=Integer,Description="Alternate allele observation count">
##INFO=<ID=DP,Number=4,Type=Integer,Description="Total read depth at the locus">
##INFO=<ID=HP,Number=5,Type=Integer,Description="Homopolymer length at the locus">
##INFO=<ID=TYPE,Number=6,Type=String,Description="The variation type: either SNV, INS or DEL">
##INFO=<ID=CONF,Number=7,Type=String,Description="Confidence of the variant. It has 5 levels: low(1/5), average(2/5), high(3/5), strong(4/5) or certain(5)">
##INFO=<ID=END,Number=8,Type=Integer,Description="Stop position of the interval">
##INFO=<ID=MEAN_DP,Number=9,Type=Integer,Description="Average depth on the interval">
##INFO=<ID=MEAN_QSCORE,Number=10,Type=Float,,Description="Average base quality score on the interval">
#CHROM	POS	ID	REF	ALT	Q-VALUE	FILTER	INFO
"""

	return l1+l2+l3+l4+l5+l6+rest



# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY INSERETD BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -READNAME          : (STR)  NAME/ID OF THE READ
# 		  -UMI               : (STR)  UMI SEQUENCE OF THE READ
# 		  -CHROM             : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -POSITION          : (INT)  THE POSITION OF THE INSERTION SITE IN THE READ
# 		  -SEQ               : (STR)  THE SEQUENCE OF THE READ
# 		  -STRAND            : (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CURSOR_POS        : (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ        : (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
# 		  -MAXX              : (INT)  THE LENGTH OF THE CIGAR 'I' ELEMENT
# 		  -QUAL              : (STR)  THE QUALITY STRING OF THE READ
# 		  -QUALS             : (DICT) A DICTIONNARY FOR THE CONVERSION OF THE QUALITIES FROM ASCII TO INT
# 		  -MIN_BASE_QUALITY  : (INT)  MINIMUM QUALITY SCORE OF THE BASE FOR IT TO ADDED TO THE PILEUP DICTIONNARY
#
# VALUE : -POSITION          : (INT)  THE FINAL POSITION THAT'S BEEN PARSED IN THE READ
#		  -CURSOR_POS        : (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ        : (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ

def AddInsertions(pileup, readName, umi, chrom, position, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY):

	# create an empty string to contain the inserted sequence
	# create a mean qscore for the inserted sequence 
	inserted_seq = ""
	inserted_qscore = 0


	# try because position in read could not be in BED file
	# therefore, the position could not be in the PILEUP dictionnary
	# if position not in BED, skip the read
	try:
		# testing if position + 1 if in the BED file
		# position is the position of the last matched/mismatched position
		# returned by the AddMatches function, therefore the insertion 
		# occurs at position+1
		test = pileup[chrom][position+1]



		# looping through the inserted sequence
		# maxx = length of the "I" cigar element => length of the inserted_sequence
		for i in range(0, maxx):
			# get base at i
			base = seq[cursor_seq]
			# get base qual at i
			baseQual = qual[cursor_seq]
			# convert base qual to base qscore
			base_qscore = quals[baseQual]

			# add the base at i in seq to the inserted sequence
			inserted_seq += base
			# increment the inserted_seq qscore with the quality 
			# of the inserted base
			inserted_qscore += base_qscore
			# advance in the read sequence
			cursor_seq += 1



		# calculate qscore for the inserted sequence corresponding
		# to the mean of the qscores of the bases in the inserted seq
		inserted_qscore = inserted_qscore/len(inserted_seq)


			
		# check if the quality of the inserted sequence >= minimum base quality
		if inserted_qscore >= MIN_BASE_QUALITY:

			# if this insertion was already seen at this location
			try:
				# try to increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position+1]['in'][inserted_seq][strand] += 1
				# add the umi to the corresponding set specific to the inserted sequence
				pileup[chrom][position+1]['in'][inserted_seq][2].append(umi)
				# add the readName to the corresponding list specific to the base in the PILEUP dictionnary
				pileup[chrom][position+1]['in'][inserted_seq][3].append(readName)


			# if this is the first time we see this inserted sequence at this position, 
			# an entry should be created with its appropriated structure (2 counters + set)  
			except:
				# create the entry in PILEUP dictionnary
				pileup[chrom][position+1]['in'][inserted_seq] = [0, 0, [umi], [readName]]
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position+1]['in'][inserted_seq][strand] += 1
			
			# increment the qScore of the in only
			pileup[chrom][position+1]['in']['qScore'] += inserted_qscore
			# increment the qScore of this position
			pileup[chrom][position+1]['base_error_probability'].append(inserted_qscore)


			cursor_pos += 1
			cursor_seq += 1

	
	# if position not in BED, skip the read
	except:
		cursor_seq += maxx
	
	# return position, cursor_seq and cursor_pos to continue from where we left off
	return [position, cursor_seq, cursor_pos]




#
# THIS FUNCTION IS USED TO DETECT PHASED VARIANTS AMONG THE FINAL LIST OF DETECTED VARIANTS
# 
# INPUT : 
#         -VARIANTS            : (DICT)   A DICTIONNARY CONTAINING THE FINAL LIST OF THE DETECTED VARIANTS
# 		  -MIN_PHASE_UMI       : (INT)    THRESHOLD FOR THE MINIMUM NUMBER OF COMMON UMI BETWEEN TWO VARIANTS TO BE CONSIDERED PHASED
# 		  -MIN_PHASE_VAF_RATIO : (FLOAT)  THRESHOLD FOR THE MINIMUM VAF RATIO OF TWO VARIANTS TO BE CONSIDERED PHASED
# 		  -MAX_PHASE_DISTANCE  : (INT)    THRESHOLD FOR THE MAXIMUM DISTANCE BETWEEN PHASED VARIANTS
#
#
# VALUE : -PHASEDVARIANTS      : (DICT)   A DICTIONNARY CONTAINING A LIST OF THE DETECTED PHASED VARIANTS AND THEIR INFOS
#



def Phase(variants, MIN_PHASE_UMI, MIN_PHASE_VAF_RATIO, MAX_PHASE_DISTANCE):



	seen = {}
	candidates = {}

	for x in variants:
		for y in variants[x]:
			chrom = variants[x][y]['line'].split('\t')[0]
			pos = int(variants[x][y]['line'].split('\t')[1])
			var = variants[x][y]['line'].split('\t')[2]
			vaf = float(variants[x][y]['line'].split('\t')[7].split(';')[1].replace('UMI_AF=', ''))
			umi = variants[x][y]['umi']

			try:
				seen[chrom][var] = {'position': pos, 'umi': umi, 'vaf': vaf}
			except:
				seen[chrom] = {var: {'position': pos, 'umi': umi, 'vaf': vaf}}







	for chrom in seen:
		x = seen[chrom]
		for var in seen[chrom]:
			y = list(x.keys())
			y.remove(var)
			pos = seen[chrom][var]['position']
			for var2 in y:
				pos2 = seen[chrom][var2]['position']
				if abs(pos2 - pos) <= MAX_PHASE_DISTANCE:
					try:
						candidates[var][var2] = {'umi1': seen[chrom][var]['umi'], 'umi2': seen[chrom][var2]['umi'], 'vaf1': seen[chrom][var]['vaf'], 'vaf2': seen[chrom][var2]['vaf']}
					except:
						candidates[var] = {var2: {'umi1': seen[chrom][var]['umi'], 'umi2': seen[chrom][var2]['umi'], 'vaf1': seen[chrom][var]['vaf'], 'vaf2': seen[chrom][var2]['vaf']}}






	final = {}





	for x in candidates:

		old_umis_in_common = []

		for y in candidates[x]:

			umi1 = set(candidates[x][y]['umi1'])
			umi2 = set(candidates[x][y]['umi2'])
			vaf1 = candidates[x][y]['vaf1']
			vaf2 = candidates[x][y]['vaf2']
			
			com = len(umi1&umi2)
			ratio = round(vaf1/vaf2, 2) if vaf1 < vaf2 else round(vaf2/vaf1, 2) 

			new_umis_in_common = old_umis_in_common.copy()
			new_umis_in_common.append(umi2)
			cumulative_com_umi = len(set.intersection(*[i for i in new_umis_in_common])) if len(old_umis_in_common) > 0 else com


			# if "23230321" in x and "23230373" in y:
			# 	print(1)
			# 	print(umi1)
			# 	print(umi2)
			# 	# print(umis_in_common)
			# 	print("\n")

			# if "23230321" in x and "23230439" in y:
			# 	print(2)
			# 	print(umi1)
			# 	print(umi2)
			# 	# print(umis_in_common)
			# 	print("\n")

			# if "27087433" in x and "27087409" in y:
			# 	print(3)
			# 	print(umi1)
			# 	print(umi2)
			# 	# print(umis_in_common)
			# 	print("\n")

			
			if com >= MIN_PHASE_UMI:
				if ratio >= MIN_PHASE_VAF_RATIO:
					final[x+" - "+y] =  str(com)+" / "+str(ratio)+" / "+str(cumulative_com_umi)

					old_umis_in_common.append(umi1)
					old_umis_in_common.append(umi2)




	# for x in final:
	# 	if '23230321G>C -' in x:
	# 		print(x)
	# 		print(final[x])




	network = {}

	for x, infos in final.items():
		x = x.split(' - ')
		var1 = x[0]
		var2 = x[1]
		infos = infos.split(' / ')

		try:
			network[var1][var2] = {'com_umi': infos[0], 'vaf_ratio': infos[1], 'cumulative_com_umi': infos[2]}
		except:
			network[var1] = {var2: {'com_umi': infos[0], 'vaf_ratio': infos[1], 'cumulative_com_umi': infos[2]}}



		# for x in network:
		# 	children = network[x]
		# 	print(x, children)
		


	return(network)







# THIS FUNCTION IS CALLED TO SPLIT THE INITIAL SAM FILE INTO N SAM SUBFILES, N CORRESPONDING
# TO THE NUMBER OF CORES USED TO EXECUTE THE PROGRAM. THEREFORE, EACH CORE WILL ANALYZE A 
# SUBFILE, ALLOWING THEREFORE ALL THE SUBFILES TO BE ANALYZED AT THE SAME TIME BY ALL THE 
# PRECISED CORES TO BE USED.
#
# INPUT : 
# 		  -FILENAME                     : (STR)    THE PATH OF THE INITIAL SAM FILE
# 		  -TOTALLINES                   : (FLOAT)  TOTAL LINES IN THE FILE
# 		  -CORES                        : (INT)    NUMBER OF CORES TO BE USED
#
# VALUE : -SUBFILES                     : (LIST)   A LIST CONTAINING THE NAMES OF THE CREATED SUBFILES
#		
#




def PreprocessReads(fileName, cores):

	# if only one core is to be used, no need to split the file
	# return the initial file
	if cores == 1:
		return [fileName]




	# print("\n")
	# PrintTime('console', "\tPreprocessing Reads...")


	# create an empty list to contain the created sub files names
	subFiles = []

	i = 0
	while i < cores:
		i_str = "0"+str(i) if i < 10 else str(i)
		subFiles.append(fileName.replace('.sam', '_'+i_str+'.sam'))
		i += 1
 

	command = "split -n l/"+str(cores)+" -d -a 2 --additional-suffix .sam "+fileName+" "+fileName.replace('.sam', '_')
	os.system(command)

	
	# print("\n")
	# PrintTime('console', "\tDone")

	# return the subfiles list 
	return subFiles




# THIS FUNCTION WILL CREATE A COVERAGE FILE FOR THE ANALYZED SAMPLE GIVING UMI DEPTH AND READ DEPTH 
# STATISTICS FOR EACH REGION OF THE PROVIDED BED FILE
# PERCENTAGE OF THE LOCATIONS THAT HAVE A DEPTH > 0.2*AVERGAE DEPTH)
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# INPUT : -BED            	 : (STR) PATH TO THE BED FILE
# INPUT : -OUTPUT            : (STR) PATH TO THE OUTPUT FOLDER
# INPUT : -SAM            	 : (STR) SAM FILE NAME
#
# VALUE : NONE
#	


def ComputeCoverageStats(pileup, BED, OUTPUT, SAM):
	# print("\n")
	# PrintTime('console', "\tComputing Coverage Statistics...")

	# create an empty dictionnary to store the statistic values for each region of the BED file
	coverageStats = {}

	# read the bed file line by line
	for line in open(BED):
		
		# remove trailing white spaces in line
		line = line.strip()

		# split the line using tab as delimiter
		line = line.split('\t')

		if len(line) >= 3:
			
			# check if the line containts at least chr, start, end and gene name
			region_value = line[3] if len(line) >= 4 else ""

			# Extract informations
			chr, start, end,  gene = line[0], int(line[1])+1, int(line[2]), region_value

			# Get genomic intervals
			intervals = list(range(start,(end+1)))
			
			UMIs = []
			reads = []

			region_UMIs={}
			
			for position in intervals:

				unique_umis = {}

				total_reads = 0

				try :
					composition = pileup[chr][position]

					# For each base, extract number of reads and 
					total_reads_pos = 0
					total_reads_neg = 0
					
					for base in ['A', 'C', 'G', 'T']:

						# extract number of reads on both strands
						total_reads_pos += composition[base][0]
						total_reads_neg += composition[base][1]

						# concatenate the lists
						for u in composition[base][2]:
							unique_umis[u] = None
							region_UMIs[u] = None

					total_reads = total_reads_pos + total_reads_neg

					#Unique UMIs set
					UMIs.append(len(unique_umis.keys()))
					reads.append(total_reads)

				except:
					UMIs.append(0)
					reads.append(0)


			
			coverageStats[chr+":"+str(start)+"-"+str(end)] = {
				
				"chr" :		chr,
				"start" :	start,
				"end" :		end,
				"fullUMIdepth" :	len(region_UMIs),
				"minUMIdepth" : 	min(UMIs),
				"maxUMIdepth" : 	max(UMIs),
				"medianUMIdepth" : 	statistics.median(UMIs),
				"meanUMIdepth" : 	statistics.mean(UMIs),
				"coverageFull" : 	sum(reads),
				"coverageMin" : 	min(reads),
				"coverageMax" : 	max(reads),
				"coverageMedian" : 	statistics.median(reads),
				"coverageMean" : 	statistics.mean(reads),
				"coverageSD" : 		statistics.stdev(reads),
				"region" : 			region_value
			}

	# open output file
	with open(OUTPUT+"/"+SAM.replace(".sam", "_amplicon_depth.csv").split("/")[-1], "w") as statsFile:
		# write header
		columns = ["seqnames", "start", "end", "width", "strand", "avgCoverage", "coverageSD", "coverageFULL", "coverageMIN", "coverageMAX", "coverageMedian", "coverageMean", "fullUMIdepth", "minUMIdepth", "maxUMIdepth", "medianUMIdepth", "meanUMIdepth", "amplicon", "region"]
		
		statsFile.write('"'+'";"'.join(columns)+'"\n')

		# for each region
		for region in coverageStats:
			# write values in file
			values = [coverageStats[region]["chr"], coverageStats[region]["start"],  coverageStats[region]["end"], (coverageStats[region]["end"] - coverageStats[region]["start"]), "*", coverageStats[region]["coverageMean"], coverageStats[region]["coverageSD"], coverageStats[region]["coverageFull"], coverageStats[region]["coverageMin"], coverageStats[region]["coverageMax"], coverageStats[region]["coverageMedian"], coverageStats[region]["coverageMean"], coverageStats[region]["fullUMIdepth"], coverageStats[region]["minUMIdepth"], coverageStats[region]["maxUMIdepth"], coverageStats[region]["medianUMIdepth"], coverageStats[region]["meanUMIdepth"], '"'+coverageStats[region]["region"]+'"', '"'+coverageStats[region]["region"]+'"']

			values = map(str, values)

			statsFile.write(';'.join(values)+"\n")

	# PrintTime('console', "\tDone")
	return coverageStats



# THIS FUNCTION WILL APPLY THE POISSON TEST ON EACH POSITION TO TRY AND FIND A SIGNIFCANT VARIANTS. THEN, P-VALUES ARE CORRECTED
# WITH THE BENJAMINI-HOCHBERG METHOD TO OBTAIN Q-VALUES. ONLY POSITIONS WITH Q-VALUE > ALPHA ARE KEPT
#
# INPUT : 
#         -PILEUP              : (DICT)   THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -ALPHA               : (FLOAT)  TYPE 1 ERROR PROBABLITY OR ALPHA LEVEL
#		  -WHITE_LIST		   : (LIST)   A LIST CONTAINING THE INDEXES OF SPECIFIC VARIANTS THAT SHOULD BE CALLED EVEN IF
#										  THEY DON'T PASS THE FILTERS
#
# VALUE : -PILEUP              : (DICT)   THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC



def FilterPositions(pileup, ALPHA, WHITE_LIST):
	print("\n")
	PrintTime('console', "\tSearching for candidate positions...")

	# define counters to calculate progress
	currentPos = 1.0
	lastProgress = 0.0
	totalPos = GetPileupLength(pileup)


	# create two dicts to contain p-values and q-values
	pValues = {}
	qValues = {}

	# create an empty array to stock positions that are not variants
	toRemove = []

	# loop through pileup
	# for each chromosome
	for chrom, infos in pileup.items():
		# for each position | composition = counts
		for position, composition in infos.items():

			# get estimated error probability for this position
			base_error_probability = composition['base_error_probability']

			# function that calculates and displays progress
			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			# calculate the estimaded lambda value for the poisson model
			estimated_lambda=int(float(base_error_probability)*composition["depth"])

			# get insertions + and - counts
			n_ins_pos = 0
			n_ins_neg = 0
			for value in composition['in'].values():
				if isinstance(value, list):
					n_ins_pos += value[0]
					n_ins_neg += value[1]
			
			# get deletions + and - counts
			n_del_pos = 0
			n_del_neg = 0
			for value in composition['del'].values():
				n_del_pos += value[0]
				n_del_neg += value[1]
			
			# create a dictionnary with total counts for only base / ins / del counts for this position 
			calls = { 
				"A": composition["A"][0]+composition["A"][1],
				"C": composition["C"][0]+composition["C"][1],
				"G": composition["G"][0]+composition["G"][1],
				"T": composition["T"][0]+composition["T"][1],
				"in": n_ins_pos+n_ins_neg,
				"del": n_del_pos+n_del_neg
			}

			# remove the reference base counts in the dictionnary
			calls.pop(pileup[chrom][position]["ref"], None)	

			# order the remaining alleles by value
			calls = OrderedDict(sorted(calls.items(), key=operator.itemgetter(1), reverse=True))

			
			# check if there is a possible second alternative allele
			pileup[chrom][position]["alt"] =list(calls.keys())[0] if list(calls.values())[0] > 0 else None
			# check if there is a possible second alternative allele
			pileup[chrom][position]["alt2"]=list(calls.keys())[1] if list(calls.values())[1] > 0 else None
			# check if there is a possible third alternative allele
			pileup[chrom][position]["alt3"]=list(calls.keys())[2] if list(calls.values())[2] > 0 else None


			if composition["depth"] <= 0 or pileup[chrom][position]["alt"] == None:
				# increment counter for progress
				currentPos += 1.0
				# if depth == 0 => remove position from pileup (means position is not covered by BAM/SAM file)
				# if the alternative allele count = 0 => no variants at this position
				toRemove.append(chrom+":"+str(position))
				continue

			
			# get the alternative allele count
			n_alt = calls[pileup[chrom][position]["alt"]]

			# calculate allelic frequency of the variant and add it to the PILEUP dictionnary	
			pileup[chrom][position]["VAF"]=float(n_alt)/composition["depth"]
			
			# calculate the pvalue for the variant to be background noise and add it to the PILEUP dictionnary
			pileup[chrom][position]["p-value"] = float(1-poisson.cdf(n_alt, estimated_lambda))
			
			# add the index and the p value to the pvalues dict 
			pValues[chrom+":"+str(position)+"-"] = pileup[chrom][position]["p-value"]

			if pileup[chrom][position]["alt2"] != None:
				# get the second alternative allele count
				n_alt = calls[pileup[chrom][position]["alt2"]]

				# calculate allelic frequency of the variant and add it to the PILEUP dictionnary	
				pileup[chrom][position]["VAF2"]=float(n_alt)/composition["depth"]
				
				# calculate the pvalue for the variant to be background noise and add it to the PILEUP dictionnary
				pileup[chrom][position]["p-value2"] = float(1-poisson.cdf(n_alt, estimated_lambda))
				
				# append the p value to the pvalues list 
				# pValues.append(pileup[chrom][position]["p-value2"])
				pValues[chrom+":"+str(position)+"-2"] = pileup[chrom][position]["p-value2"]



			if pileup[chrom][position]["alt3"] != None:
				# get the third alternative allele count
				n_alt = calls[pileup[chrom][position]["alt3"]]

				# calculate allelic frequency of the variant and add it to the PILEUP dictionnary	
				pileup[chrom][position]["VAF3"]=float(n_alt)/composition["depth"]
				
				# calculate the pvalue for the variant to be background noise and add it to the PILEUP dictionnary
				pileup[chrom][position]["p-value3"] = float(1-poisson.cdf(n_alt, estimated_lambda))
				
				# append the p value to the pvalues list 
				# pValues.append(pileup[chrom][position]["p-value3"])
				pValues[chrom+":"+str(position)+"-3"] = pileup[chrom][position]["p-value3"]



			currentPos += 1.0





	# remove unwanted positions
	for index in toRemove:
		chrom = index.split(":")[0]
		position = int(index.split(":")[1])

		pileup[chrom].pop(position, None)




	# reset toRemove array
	toRemove = []

	# recalculate pileup length
	totalPos = GetPileupLength(pileup)

	# apply FDR Correction (Benjamini/Hochberg Correction) to correct pvalues 
	# based on multiple test theory
	try:
		corrected = smm.multipletests(list(pValues.values()), alpha=0.05, method='fdr_bh')
		
		# retrieve results of the correction
		# retieve results just in case we would need it
		results = corrected[0]
		qValues_list = corrected[1]
		foundCandidates = True
	except:
		results = []
		qValues_list = []
		foundCandidates = False




	# retrieve qValues
	counter = 0
	for index in pValues.keys():
		qValues[index] = qValues_list[counter]
		counter += 1


	# loop the the qValues dict and the each qValues
	# to its corresponding position in the pileup
	for index, qvalue in qValues.items():
		index = index.split(":")
		chrom = index[0]
		position = int(index[1].split("-")[0])
		alt_index = index[1].split("-")[1]

		pileup[chrom][position]['q-value'+alt_index] = qvalue





	currentPos = 1.0

	# loop through pileup
	# for each chromosome
	for chrom, infos in pileup.items():
		# for each position | composition = counts		
		for position, composition in infos.items():
			# function to calculate and display progress
			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)


			# if qvalue >= alpha(default = 0.05) => consider as artifact
			if pileup[chrom][position]["q-value"] >= ALPHA and chrom+":"+str(position) not in WHITE_LIST['pos']:

				if chrom+":"+str(position-1) not in WHITE_LIST['pos'] and chrom+":"+str(position-1)+">del" not in WHITE_LIST['var'] and chrom+":"+str(position-1)+">in" not in WHITE_LIST['var']:
					# remove position from pileup dictionnary
					toRemove.append(chrom+":"+str(position))
			else:
				# if there is a possible second allele variant
				if composition["alt2"] != None:
					# if qvalue2 >= alpha(default = 0.05) => consider as artifact
					if pileup[chrom][position]["q-value2"] >= ALPHA and chrom+":"+str(position) not in WHITE_LIST['pos']:
						if chrom+":"+str(position-1) not in WHITE_LIST['pos'] and chrom+":"+str(position-1)+">del" not in WHITE_LIST['var'] and chrom+":"+str(position-1)+">in" not in WHITE_LIST['var']:
							pileup[chrom][position]["alt2"] = None


				# if there is a possible third allele variant
				if composition["alt3"] != None:
					# if qvalue2 >= alpha(default = 0.05) => consider as artifact
					if pileup[chrom][position]["q-value3"] >= ALPHA and chrom+":"+str(position) not in WHITE_LIST['pos']:
						if chrom+":"+str(position-1) not in WHITE_LIST['pos'] and chrom+":"+str(position-1)+">del" not in WHITE_LIST['var'] and chrom+":"+str(position-1)+">in" not in WHITE_LIST['var']:
							pileup[chrom][position]["alt3"] = None







			# increment counter for progress
			currentPos += 1




	# remove unwanted positions
	for index in toRemove:
		chrom = index.split(":")[0]
		position = int(index.split(":")[1])

		pileup[chrom].pop(position, None)



	# get total kept positions
	potential = GetTotalVariants(pileup)


	print("\n")
	PrintTime('console', "\tDone")




	# return PILEUP dictionnary
	return [pileup, potential, foundCandidates]



	




def PrintHelp(tool, VERSION, LAST_UPDATE):

	tools = ["general", "extract", "call"]

	messages= []
	message = """
GENERAL DESCRIPTION
===============================================

Software for analyzing BAM/SAM files produced by an Illumina MiSeq sequencer using UMI (Unique Molecular Identifiers).
The software starts by building a pileup based on the regions specified in the BED file. This software integrates an  
extraction tool that can be used to extract UMI tags from the reads. The software has also a calling tool for calling 
variants and that outputs the results under VCF format.
Although SAM files generated by an Illumina MiSeq sequencer were used to develop this tool, it should work on BAM/SAM 
files generated by any other sequencer as long as they respect the Sequence Alignment/Map Format Specification.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Author: Pierre-Julien VIAILLY, Centre Henri Becquerel, Rouen, France
:Version: """+VERSION+"""
:Last updated: """+LAST_UPDATE+"""
:Tags: Genomics, UMI, Illumina, Miseq, Variant Calling


There are 2 tools available in this software:

	-extract
	-call


To get help on a specific tool, type:

	python3 umi-varcal.py <tool>
	or 
	python3 umi-varcal.py <tool> --help
	or 
	python3 umi-varcal.py <tool> -h

To use a specific tool, type:

	python3 umi-varcal.py <tool> [tool arguments]\n\n"""

	messages.append(message)





	message = """
TOOL DESCRIPTION : extract
===============================================

The extraction tool allows to extract the UMI sequence from the SAM/BAM file 
and add it to the end of the read ID, preceeded by an '_'. 
Two extraction modes are available: extraction from the start of the sequence 
and extraction from the BAM/SAM file RX field. Please refer to the umi_length 
parameter to choose between the 2 modes.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Author: Pierre-Julien VIAILLY, Centre Henri Becquerel, Rouen, France
:Version: """+VERSION+"""
:Last updated: """+LAST_UPDATE+"""
:Tags: Genomics, UMI, Illumina, Miseq, Variant Calling, Alignment


USAGE
===============================================

python3 umi-varcal.py extract [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-bwa
-Python module: pysam


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	Spaces are not allowed in the file path.
	For example : /home/my_dir/my_subdir/hg19.fasta

-i, --input=FILE (case sensitive)
	The input file SAM/BAM generated by the sequencer.
	It is recommended that you enter the input file full path
	Spaces are not allowed in the file path.
	For example : /home/my_dir/my_subdir/ind1.sam

-l, --umi_length=INT 
	umi_length : the length of the UMI sequence to extract
	The value must be a valid integer
	If the UMI tag is in the RX field, set umi_length to 0


OPTIONAL ARGUMENTS
===============================================

-t, --bwa_threads=INT 
	The number of cores to be used for the bwa alignment. Using more
	cores will significantly reduce the alignment time.
	The value of this parameter defaults to 1


EXAMPLE
===============================================

python3 umi-varcal.py extract -i /home/.../test.sam -f /home/.../hg19.fa -l 12 -t 5


OUTPUT
===============================================

The tool will automatically create a directory in the current directory 
called EXTRACTED. It will contain a folder named after the sample input.
In this directory, you can find the extracted R1 FASTQ, the extracted R2
FASTQ, the extracted BAM (with its index) and the extracted SAM file.\n\n"""

	messages.append(message)






	message = """
TOOL DESCRIPTION : call
===============================================

A tool for calling variants in a SAM/BAM file from which the UMI tags were extracted.
The calling tool starts by building a pileup based on the regions specified in the BED file. Given that the UMI tags
had already been extracted correctly, this tool will call variants and output the results under VCF format.
Although BAM/SAM files generated by an Illumina MiSeq sequencer were used to develop this tool, it should work on 
BAM/SAM files generated by any other sequencer as long as they respect the Sequence Alignment/Map Format Specification.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Author: Pierre-Julien VIAILLY, Centre Henri Becquerel, Rouen, France
:Version: """+VERSION+"""
:Last updated: """+LAST_UPDATE+"""
:Tags: Genomics, UMI, Illumina, Miseq, Variant Calling, Alignment


USAGE
===============================================

python3 umi-varcal.py call [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-Python modules:   
					cython
                    msgpack
                    pysam
                    scipy
                    statsmodels.stats.multitest


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	Spaces are not allowed in the file path.
	For example : /home/my_dir/my_subdir/hg19.fasta

-b, --bed=FILE (case sensitive)
	The panel design file in a BED format.
	It is recommended that you enter the BED file full path
	Spaces are not allowed in the file path.
	For example : /home/my_dir/my_subdir/design.bed

-i, --input=FILE (case sensitive)
	The input file SAM/BAM generated by the sequencer and from which
	the UMI tags have already been extracted from the sequences and
	and added at the end of the read ID, preceded by an '_'. This format
	is the only format accepted by the variant caller.
	It is recommended that you enter the input file full path
	Spaces are not allowed in the file path.
	For example : /home/my_dir/my_subdir/ind1.sam



OPTIONAL ARGUMENTS
===============================================

-p, --pileup=FILE (case sensitive)
	The PILEUP file for the SAM file previously dumped during
	a older analysis with the same BED and FASTA files. This 
	is not the pileup format generated by SAMtools. If specified,
	the tool will attempt te retrieve the counts of each position
	from the PILEUP file instead of rebuilding the pileup from 
	scratch, a process that is relatively fast and can save time.
	It is recommended that you enter the PILEUP file full path
	Spaces are not allowed in the file path.
	For example : /home/my_dir/my_subdir/ind1.pileup

-o, --output=DIR (case sensitive)
	The output directory in which your files will be generated.
	It is recommended that you enter the output dir full path
	For example : /home/my_dir/my_subdir/out_dir

-c, --cores=INT
	The number of cores to be used for the analysis.
	Using more cores won't necessarily reduce analysis time but
	will significantly increase RAM consumption. Using more cores
	is advantageous with smaller panels and larger SAM/BAM files.
	(Check documentation for more info).
	The value of this parameter defaults to 1.

--min_base_quality=INT
	The minimum value for which a base with a certain quality is
	considered as "good". Only bases with a quality >= min_base_quality
	will be considered.
	The value of this parameter defaults to 10 

--min_read_quality=INT
	The minimum value for which a read with a certain mean quality is
	considered as "good". Only reads with a quality >= min_read_quality
	will be considered.
	The value of this parameter defaults to 20 

--min_mapping_quality=INT
	The minimum value for which a read with a certain mapping 
	quality is considered as "good". Only bases with a mapping
	quality >= min_mapping_quality will be considered.
	The value of this parameter defaults to 20

--min_variant_depth=INT
	The minimum count for a variant with a certain raw count to be
	called. Only variants with a raw count >= min_variant_depth 
	will be called.
	The value of this parameter defaults to 5

--alpha=INT/FLOAT
	This is the type I error probability known as alpha level.
	Positions with a p-value/q-value >= alpha will be considered as
	noisy and variants in these positions won't be called.
	The value of this parameter defaults to 0.05 (5%)

--min_variant_umi=INT
	The minimum count for a variant with a certain unique UMIs 
	count to be called. Only variants with a unique UMI count >= 
	min_variant_umi will be called.
	The value of this parameter defaults to 5

--strand_bias_method=STR
	This parameter lets you specifiy the method you want to use
	to calculate variant strand bias. The value can only be 
	"default" or "torrent_suite". Choosing any of the methods 
	can have minimal effect on the final variants called and the SB
	value differs: the torrent suite method generates SB values [0.5;1]
	but the default method generates SB values [0;+Inf].
	Obviously, this parameter defaults to the value "default"

--max_strand_bias=INT/FLOAT
	The minimum strand bias value for a variant with a certain  
	negative and positive coverage to be called. Only variants 
	with a strand bias <= max_strand_bias will be called.
	The value of this parameter defaults to 1.0 for the default
	method and to 0.743 for the torrent suite method

--max_hp_length=INT
	The maximum homopolymer length for a variant at a certain  
	position in an homopolymer region to be called. Only variants 
	with a homopolymer length <= max_hp_length will be called.
	The value of this parameter defaults to 10

--gvcf=BOOL
	This is an experimental feature that is turned off by default
	and that, if set to True, will generate a gVCF file along
	the normal VCF file

--keep_pileup=BOOL
	This lets the user keep or delete the generated pileup file.
	By default, this parameter is set to True

--ignore_monocluster_umi=BOOL
	UMI read groups containing only R1 and R2 reads of the same
	cluster will be treated as a singleton if this parameter is
	set to True. The UMI tags will not be considered as concordant.
	By default, this parameter is set to False

--compute_coverage_stats=BOOL
	UMI-VarCal will generate a file with UMI coverage statistics 
	per BED region and per position.
	By default, this parameter is set to False

--black_list=FILE
	A .txt file containing a list of variants respecting the HGVS
	format. The file must contain one variant per line only. The 
	variants in this file are black listed and will not appear in
	the final VCF file even if they pass all the filters
	It is recommended that you enter the file full path
	Spaces are not allowed in the file path.

--white_list=FILE
	A .txt file containing a list of variants respecting the HGVS
	format. The file must contain one variant per line only. The 
	variants in this file are white listed and will be shown in
	the final VCF file even if they don't pass any of the filters.
	The same position can not be black and white listed at the same
	time: If the same location is found in the black and the white
	list, the position will be analyzed with the chosen filters
	It is recommended that you enter the file full path
	Spaces are not allowed in the file path.

--min_phase_umi=INT
	The minimum number of commom UMI tags for phase variants to be
	called. Only variants with a common UMI count >= min_phase_umi 
	will be called. The value of this parameter defaults to 3

--min_phase_vaf_ratio=FLOAT
	The minimum allelic frequency ratio between phase variants to be
	called. Only variants with a VAF ratio >= min_phase_umi will be 
	called. The value of this parameter defaults to 0.8

--max_phase_distance=INT
	The maximum distance allowed between two phased variants. Only 
	phased variants with a distance <= max_phase_distance will be 
	called. The value of this parameter defaults to 150


EXAMPLE
===============================================

python3 umi-varcal.py call -i /home/.../test.sam -b=/home/.../design.bed -f /home/.../hg19.fa -p=/home/.../test.pileup -o /home/.../out -c=3 --min_base_quality 15 --min_read_quality 25 --min_mapping_quality 40 --min_variant_depth 8 --min_variant_umi 7 --strand_bias_method default --max_strand_bias 0.8 --alpha 0.01 --max_hp_length 5 --gvcf True --keep_pileup False --ignore_monocluster_umi=False --black_list black_list.txt --white_list=white_list.txt --min_phase_umi 5 --min_phase_vaf_ratio=0.85 --max_phase_distance 135 --compute_coverage_stats=True



OUTPUT
===============================================

1-  A VCF file (<INPUT_NAME>.vcf) containing all the variants that
    were successfully called.

2-  A gVCF file (<INPUT_NAME>.gvcf) containing all the variants that
    were successfully called + informations for the blocks in which
    no variant was detected.
	
3-  A VARIANTS file (<INPUT_NAME>.variants) containing all the variants
    that were successfully called. This file contains the same variants of
    the VCF file, in addition to detailed metrics about the variants.

4-  A PILEUP file (<INPUT_NAME>.pileup) corresponding to the entire pileup dumped.
    This file is produced in the output directory. This file can be used to skip 
    the pileup building and load it instead if the analysis was already done.\n\n"""

	messages.append(message)



	PrintProgramName()
	print(messages[tools.index(tool)])

	exit()


# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY DELETED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -READNAME          : (STR)  NAME/ID OF THE READ
# 		  -UMI               : (STR)  UMI SEQUENCE OF THE READ
# 		  -STRAND            : (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CHROM             : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -START             : (INT)  THE START POSITION OF THE READ
#		  -CIGAR             : (STR)  THE CIGAR SEQUENCE OF THE READ
# 		  -SEQ               : (STR)  THE SEQUENCE OF THE READ
# 		  -QUAL              : (STR)  THE QUALITY STRING OF THE READ
# 		  -QUALS             : (DICT) A DICTIONNARY FOR THE CONVERSION OF THE QUALITIES FROM ASCII TO INT
# 		  -MIN_BASE_QUALITY  : (INT)  MINIMUM QUALITY SCORE OF THE BASE FOR IT TO ADDED TO THE PILEUP DICTIONNARY
#
# VALUE : NONE
#		  




def AddRead(pileup, readName, umi, strand, chrom, start, cigar, seq, qual, quals, MIN_BASE_QUALITY):


	# split the cigar sequence using any of the five characters as a delimiter:
	# M (match/mismatch), S (soft clip), I (insertion), 
	# D (deletion) or H (hard clip)
	# this will return a list with the lengths of the consecutive events
	cigar_lengths = re.split('M|S|I|D|H', cigar)
	
	# try removing the empty element at the end of the list created by splitting
	# if last character is empty
	try:
		# remove it
		cigar_lengths.remove('')

	# if last character is not empty
	# a character not in [M,I,S,D,H] is in the cigar element
	# don't know what to do => skip read 
	except:
		print('\n')
		PrintTime("error", "\t\tUnexpected character found in CIGAR ("+cigar+")... Read skipped !\n")
		return False


	# split the cigar sequence by any number
	# this will return a list with the consecutive events
	cigar_chars = re.split('[0-9]+', cigar)
	
	# remove the empty element at the end of the list created by splitting
	cigar_chars.remove('')
	
	# remove N fragment and its length from cigar if 'N' in cigar:
	if "N" in cigar:
		cigar_lengths.remove(cigar_lengths[cigar_chars.index('N')])
		cigar_chars.remove('N')

	# initialize a counter to increment position only without advancing in the sequence
	cursor_pos = 0
	
	# initialize a counter to advance in the sequence without incrementing the position
	cursor_seq = 0	 

	# the need of two seperate is for cases when the event is an insertion as we need to
	# advance in the sequence of the read without incrementing the position or when the 
	# event is a deletion as we need to increment the position without advancing in the
	# sequence of the read



	# for each cigar event in the cigar events list (M|S|I|D|H)
	for i in range(0, len(cigar_chars)):
		
		# if the event is a match/mismatch
		if cigar_chars[i] == "M":
			
			# get the length of the match/mismatch event
			maxx = int(cigar_lengths[i])

			# call the specific function responsible of parsing a match/mismatch segment of the read 
			value = AddMatches(pileup, readName, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY)
			
			# get the returned values to continue from the position where the event ends
			position = value[0]
			cursor_seq = value[1]
			cursor_pos = value[2]
		
		# if the event is an insertion
		elif cigar_chars[i] == "I":
			
			# get the length of the insertion event
			maxx = int(cigar_lengths[i])


			# try to get position
			# normally, an read does not start with an insertion so the position of the insertion
			# is the last position of the previous cigar element 
			try:
				test = position

			# in some cases, the read starts with an insertion, therefore that position is not defined
			# in this case, an exception will be throw in order to attribute the start value to position
			except:
				position = start

			# call the specific function responsible of parsing an inserted segment of the read 
			value = AddInsertions(pileup, readName, umi, chrom, position, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY)
			
			# get the returned values to continue from the position where the event ends
			position = value[0]
			cursor_seq = value[1]
			cursor_pos = value[2]

		# if the event is a deletion
		elif cigar_chars[i] == "D":

			# get the length of the deletion event
			maxx = int(cigar_lengths[i])

			# call the specific function responsible of parsing a deleted segment of the read 
			value = AddDeletions(pileup, readName, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx)

			# get the returned values to continue from the position where the event ends
			position = value[0]
			cursor_seq = value[1]
			cursor_pos = value[2]
		
		# if the event is a soft clip
		# soft clipping removes the clipped sequence from the read without correcting read start position
		elif cigar_chars[i] == "S":
			
			# test if the soft clip occurs at the beginning of the read
			try:
				# if not beginning of the read => cursor_pos > 0
				test = 20 / cursor_pos
				# in this case, do nothing since the soft clipping occured at the end of the read
				# since a clipping event can only occurs at the start or at the end of the read 
				continue

			# if beginning of the read => cursor_pos = 0 => exception 
			except:
				# get length of the clipped sequence
				clipped = int(cigar_lengths[i])
				# correct the read start position
				start -= clipped

				# increment the 2 cursors and continue
				cursor_pos += clipped
				cursor_seq += clipped

		# if the event is a hard clip
		# just continue (hard clipping corrects read start position and removes the clipped sequence from the read)
		elif cigar_chars[i] == "H":
			continue







def TreatReads(SAM, nReads, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY):

	validReads = 0

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}

	a = {'A' : '1', 'C': '2', 'G':'3', 'T':'4'}

	i=0
	for q in quals_str:
		quals[quals_str[i]] = i
		i += 1


	# pileup = ParseBED(BED)

	with open(OUTPUT+"/.pileup_ini", 'rb') as handle:
		pileup = msgpack.unpack(handle, encoding="utf-8")
		# pileup = pickle.load(handle)
	
	pileup = SortPileup(pileup)


	currentLine = 1.0
	lastProgress = 0.0
	# totalLines = GetTotalLines(SAM)
	totalLines = nReads


	ALL_UMIS = {}

	
	for line in open(SAM):

		lastProgress = PrintProgress(currentLine, totalLines, lastProgress)

		currentLine += 1


		line = line.split('\t')

		readName = line[0].split('_')

		
		try:
			umi = readName[-1]
			readName = "_".join(readName[:-1])
			
			readName = readName.split(':')[-2:]
			readName = list(map(int, readName))
			readName = readName[0]*readName[1]

			# readName = ":".join(readName)




			

		except:
			continue


		try:
			flag = int(line[1])
		except:
			continue
		
		# get strand : 0 = forward | 1 = reverse
		strand = int("{0:b}".format(int(flag))[-5])
		firstInPair = bool(int("{0:b}".format(int(flag))[-7]))
		chrom = line[2]
		pos = line[3]
		mapq = int(line[4])
		cigar = line[5]
		seq = line[9]
		qual = line[10]


		matePos = line[7]
		umiPos = int(str(pos)[-4:]) if strand == 0 else int(str(matePos)[-4:])
		

		ini_umi = umi+"-"+str(umiPos)
		found = False

		for i in range(umiPos - 3, umiPos + 3):
			test_umi = umi+"-"+str(i)
			if test_umi in ALL_UMIS:
				umi = test_umi
				ALL_UMIS[test_umi] += 1
				found = True
				break
		
		if not found:
			umi = umi+"-"+str(umiPos)
			ALL_UMIS[umi] = 1


		if ReadIsValid(flag, chrom, pos, umi, cigar, mapq, MIN_MAPPING_QUALITY, qual, quals, MIN_READ_QUALITY):
			
			AddRead(pileup, readName, umi, strand, chrom, int(pos), cigar, seq, qual, quals, MIN_BASE_QUALITY)
			validReads += 1



	return [pileup, validReads]







def Extract(config):

	# retrieve the configuration parameters
	INPUT = config['input']
	FASTA = config['fasta']
	umi_length = int(config['umi_length'])
	bwa_threads = int(config['bwa_threads'])


	# print the configuration inthe console
	PrintTime("green", "\t\tINPUT file   : "+INPUT)
	PrintTime("green", "\t\tFASTA file   : "+FASTA)
	PrintTime("green", "\t\tUMI length   : "+str(umi_length))
	PrintTime("green", "\t\tBWA Threads  : "+str(bwa_threads))

	PrintTime("console", "\tDone\n")


	# if input is bam => launch samtools view command
	# to convert it to sam
	if ".bam" in INPUT and ".sam" not in INPUT:

		print("\n")
		PrintTime('console', "\tConverting BAM to SAM...")

		SAM = BAMtoSAM(INPUT)

		PrintTime('console', "\tDone")

	else:
		# else => sam = input
		SAM = INPUT




	# get total number of reads
	totalLines = GetTotalLines(SAM)



	# build the output directories and files
	samName = SAM.split('/')[-1].replace('.sam', '')
	raw_sam = 'EXTRACTED/'+samName+"/"+samName+'_extracted.sam'
	raw_bam = 'EXTRACTED/'+samName+"/"+samName+'_extracted.bam'
	sorted_raw_bam = 'EXTRACTED/'+samName+"/"+samName+'_extracted_sorted.bam'
	try:
		os.mkdir('EXTRACTED')
	except:
		pass

	try:
		os.mkdir('EXTRACTED/'+samName)
	except:
		pass
	FASTQ_1 = 'EXTRACTED/'+samName+"/"+samName+'_R1.fastq'
	FASTQ_2 = 'EXTRACTED/'+samName+"/"+samName+'_R2.fastq'

	output1 = open(FASTQ_1, 'w')
	output2 = open(FASTQ_2, 'w')



	# create an empty dictionnary to contain read ids that were already seen
	seen = {}


	nLine = 1.0
	nReads = 0
	skipped = 0
	lastProgress = 0.0

	PrintTime('console', "\tReverting to FASTQ...")

	# loop through the sam file
	for line in open(SAM):



		# print progress
		lastProgress = PrintProgress(nLine, totalLines, lastProgress)

		# if line is not in header
		if "@SQ" not in line and "SN:chr" not in line and "LN:" not in line:

			line = line.split('\t')

			# if line corresponds to a read
			if len(line) > 10:

				# define umi
				umi = None

				# get read if and add @ to create fastq read id
				tmp_readID = "@"+line[0]
				
				# get sequence of read
				tmp_seq = line[9]
				
				# umi to be extracted from start of sequence if umi_length > 0
				if umi_length > 0:
					umi = tmp_seq[:umi_length]
				# umi to be extracted from RX tag if umi_length = 0
				else:
					for x in line:
						if 'RX:Z' in x:
							umi = x.replace('RX:Z:', '')

					if not umi:
						skipped += 1
						nReads += 1
						nLine += 1
						continue


				seq = tmp_seq[umi_length:]

				
				# extract umi quality from quality string
				tmp_qual = line[10]
				qual = tmp_qual[umi_length:]

				# add umi to the end of the read id
				readID = tmp_readID+"_"+str(umi)

				# build the fastq read
				fq_line = readID+"\n"+seq+"\n+\n"+qual+"\n"
				
				# if the read id is already in seen dict
				try:
					# retrieve the value in seen as Read 1
					fq_line1 = seen[readID]
					# write in R1 file the value in seen
					output1.write(fq_line1)
					# write in R2 file the value in the current line
					output2.write(fq_line)
					# delete the entry from seen
					del seen[readID] 

				except:
					# if the read id is not in seen dict
					# add it to the dict
					seen[readID] = fq_line

				# increment number of reads
				nReads += 1

		# increment the ine counter to track progress
		nLine += 1

	# close the fastq r1 and fastq r2 files
	output1.close()
	output2.close()


	print('\r\n')
	PrintTime('console', "\tDone")
	print('\n\r')

	if skipped > 0:
		PrintTime('warning', "\tWarning: umi_length set to 0 so extractiong UMI tags from RX field !\n\t\t\t"+str(skipped)+"/"+str(nReads)+" were skipped since no RX:Z: tag was found !\n")


	PrintTime('console', "\tAligning FASTQ to reference genome...")
	# launch bwa to do the alignment : FASTQ_R1 + FASTQ_R2 + FASTA ==> SAM
	os.system("bwa mem -M -t "+str(bwa_threads)+" "+FASTA+" "+FASTQ_1+" "+FASTQ_2+" > "+raw_sam+" 2> /dev/null")
	PrintTime('console', "\tDone")
	print('\n\r')

	PrintTime('console', "\tConverting to BAM...")
	# convert to BAM with samtools
	os.system("samtools view -S -b "+raw_sam+" > "+raw_bam+" 2> /dev/null")
	PrintTime('console', "\tDone")
	print('\n\r')

	PrintTime('console', "\tSorting BAM...")
	# sort bam with samtools 
	os.system("samtools sort "+raw_bam+" > "+sorted_raw_bam+" 2> /dev/null")
	PrintTime('console', "\tDone")
	# remove unsorted bam after sorting
	os.remove(raw_bam)
	os.rename(sorted_raw_bam, raw_bam)
	print('\n\r')

	PrintTime('console', "\tIndexing BAM...")
	# indexing bam with samtools 
	os.system("samtools index "+raw_bam+" 2> /dev/null")
	PrintTime('console', "\tDone")

# THIS FUNCTION WILL CALCULATE THREE STATISTICS : THE PERCENTAGE OF THE BED COVERED BY THE BAM/SAM FILE, 
# THE AVERAGE DEPTH OF THE BAM/SAM FILE ACROSS ALL POSITIONS AND THE UNIFORMITY OF THE SEQUENCING (THE 
# PERCENTAGE OF THE LOCATIONS THAT HAVE A DEPTH > 0.2*AVERGAE DEPTH)
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		  -POTENTIAL         : (INT)  NUMBER OF POTENTIAL VARIANTS FOUND
#		  -FINAL   			 : (INT)  NUMBER OF FINAL VARIANTS FOUND
#
# VALUE : NONE
#	


def CalculateStats(pileup, potential, final, pileupLen, nReads, validReads, REBUILD):
	print("\n")
	PrintTime('console', "\tCalculating statistics...")

	# initiate a counter for not covered positions
	not_covered = 0
	# get total number of positions in pileup
	totalPos = GetPileupLength(pileup)
	# create an empty list to stock depths
	depths = []

	# loop through the pileup items
	# for each chromosome
	for chrom , infos in pileup.items():
		# for each position | composition = counts
		for position, composition in infos.items():
			# try-except block
			# if position is covered => depth must be > 0
			try:		
				# calculate insertion counts
				n_ins_pos = 0
				n_ins_neg = 0
				for value in composition['in'].values():
					if isinstance(value, list):
						n_ins_pos += value[0]
						n_ins_neg += value[1]
				
				# calculate deletion counts
				n_del_pos = 0
				n_del_neg = 0
				for value in composition['del'].values():
					n_del_pos += value[0]
					n_del_neg += value[1]
				
				# insertions shouldn't be taken into account when calculating depth
				# n_reads = composition['A'][0]+composition['C'][0]+composition['G'][0]+composition['T'][0]+composition['A'][1]+composition['C'][1]+composition['G'][1]+composition['T'][1]+n_ins_pos+n_ins_neg+n_del_pos+n_del_neg
				n_reads = composition['A'][0]+composition['C'][0]+composition['G'][0]+composition['T'][0]+composition['A'][1]+composition['C'][1]+composition['G'][1]+composition['T'][1]+n_del_pos+n_del_neg
				# make a test to check that depth > 0
				test = 25 / n_reads
				# if test succeed => position covered => append depth to list of depths
				depths.append(n_reads)

			# if position not covered => increment not_covered counter
			except:
				not_covered += 1

	# calculate coverage
	coverage = round(float(totalPos-not_covered)/float(totalPos)*100, 2)
	# calculate average depth
	avg_depth = int(round(float(sum(depths))/float(len(depths)), 0))

	# calculate uniformity
	uniform = 0
	for depth in depths:
		if depth >= 0.2*avg_depth:
			uniform += 1
	uniformity = round(float(uniform)/float(len(depths))*100, 2)


	# print out stats to console
	message = "Total Number of Reads: "+Format(nReads)
	PrintTime('green', "\t\t"+message)
	if REBUILD:
		message = "Number of Analyzed Reads: "+Format(validReads)+" ("+str(round(float(validReads)/float(nReads)*100, 2))+" %)"
	else:
		message = "Number of Analyzed Reads: N/A (Pileup was loaded!)"
	PrintTime('green', "\t\t"+message)
	message = "Total Panel Positions: "+Format(pileupLen)
	PrintTime('green', "\t\t"+message)
	message = "BAM/BED coverage: "+str(coverage)+" %"
	PrintTime('green', "\t\t"+message)
	message = "Average Design Depth: "+Format(avg_depth)+"x"
	PrintTime('green', "\t\t"+message)
	message = "Uniformity: "+str(uniformity)+" %"
	PrintTime('green', "\t\t"+message)
	message = "Candidate Positions: "+Format(potential)
	PrintTime('green', "\t\t"+message)
	message = "Final Variants: "+Format(sum(final))
	PrintTime('green', "\t\t"+message)	
	message = "Total SUB: "+Format(final[0])
	PrintTime('green', "\t\t"+message)
	message = "Total DEL: "+Format(final[1])
	PrintTime('green', "\t\t"+message)
	message = "Total INS: "+Format(final[2])
	PrintTime('green', "\t\t"+message)


# THIS FILE CONTAINS GENERAL FUNCTIONS THAT ARE NEEDED TO 
# SUCCESSFULLY CALL THE VARIANTS IN THE SAM FILE 




# a class to define colors for each scenario
class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

# a function to print status messages + the time 
# + a specific color for each status
def PrintTime(nature, message, *args):
		
		if nature == "console":
			c_start = bcolors.BOLD+bcolors.OKBLUE
			c_end = bcolors.ENDC
		elif nature == "warning":
			c_start = bcolors.BOLD+bcolors.WARNING
			c_end = bcolors.ENDC
		elif nature == "error":
			c_start = bcolors.BOLD+bcolors.FAIL
			c_end = bcolors.ENDC
		elif nature == "red":
			c_start = bcolors.BOLD+bcolors.FAIL
			c_end = bcolors.ENDC
		elif nature == "green":
			c_start = bcolors.BOLD+bcolors.OKGREEN
			c_end = bcolors.ENDC
		
		if args:
			message = message % args
			
		print("[ " + time.strftime('%X') + " ]\t"+c_start+ message + c_end)
		sys.stdout.flush()
		sys.stderr.flush()


# this function will return a list with elements that 
# only occured once
def GetSingletons(l):
	d = defaultdict(int)
	for x in l: d[x] += 1
	k = [x for x in l if d[x] == 1]
	return k

# this function will return a list with elements that 
# occured more than once
def RemoveSingletons(l):
	d = defaultdict(int)
	for x in l: d[x] += 1
	k = [x for x in l if d[x] > 1]
	return k

# this function will return the number of singleton UMI (
# UMI found on one read only), the number of monocluster UMI
# (UMI found on 2 reads from the same cluster) and the number of
# polycluster UMI (UMI found on at least 2 reads from different clusters)
# if IGNORE_MONOCLUSTER_UMIS is True => the function will remove 
# monocluster umis from alt_umi list
def TreatSingletons(umi_list, readNames, IGNORE_MONOCLUSTER_UMI):

	result = {}
	i = 0
	n_singleton_UMI = 0
	n_monocluster_UMI = 0
	n_polycluster_UMI = 0

	for umi in umi_list:
		readName = readNames[i]
		try:
			result[umi][readName] += 1
		except:
			try:
				result[umi][readName] = 1
			except:
				result[umi] = {readName : 1}
		i += 1
	
	x = set(umi_list)

	for umi in result:

		nReadNames = len(result[umi])

		if nReadNames > 1:
			n_polycluster_UMI += 1
		else:
			readName = list(result[umi].keys())[0]
			freq = result[umi][readName]
			if freq == 1:
				n_singleton_UMI += 1
				x.remove(umi)
			else:
				n_monocluster_UMI += 1
				if IGNORE_MONOCLUSTER_UMI:
					x.remove(umi)
	
	return(x, n_singleton_UMI, n_monocluster_UMI, n_polycluster_UMI)

	
# function that calculates and displays progress efficiently
def PrintProgress(currentRead, totalReads, lastProgress,):
	progress = int((currentRead/totalReads*100))
	
	if int(progress) > int(lastProgress):
		### write progress in status file
		sys.stdout.write("\r[ " + time.strftime('%X') + " ]\t\t\tWorking "+str(int(progress))+" %")
		sys.stdout.flush()
		# time.sleep(0.001)

	return progress


# function to retieve intial RAM Usage and the exact launching time of the script 
def Start_dev(path):
	startTime = datetime.datetime.now()
	tmp_name = '.'+str(int(time.time()))
	os.mkdir(tmp_name)
	os.system('python3 '+path+'/RAM.py '+tmp_name+' &')
	return [tmp_name, startTime]




# function to retieve final RAM Usage and the exact completion time of the script 
# it will then calculate RAM Usage of the program and its execution time
# finally, it will displays the infos in the ocnsole and exit
def Exit_dev(tmp_name, startTime):
	
	# gen total time
	endTime = datetime.datetime.now()
	totalTime = endTime - startTime

	# get ram usage
	usedRAM = 0.0
	for x in os.listdir(tmp_name):
		if ".ram" in x:
			usedRAM = float(x[1:-4])
			break

	stop = open(str(tmp_name)+'/.done', 'w')
	stop.close()

	message = "\n\t\t\tElapsed time :  "+str(totalTime)
	PrintTime("console", message)

	try:

		message = "\tRAM USAGE    :  "+str(round(usedRAM, 2))+" GB"+"\n\n"
		PrintTime("console", message)

		time.sleep(1)
		for x in os.listdir(tmp_name):
			os.remove(tmp_name+"/"+x)
		
		os.rmdir(tmp_name)	
	
	except:
		print("\n")

	exit()


def Start(path):

	startTime = datetime.datetime.now()
	tmp_name = '.'+str(int(time.time()))
	return [tmp_name, startTime]



def Exit(tmp_name, startTime):
	
	endTime = datetime.datetime.now()
	totalTime = endTime - startTime

	message = "\t\tElapsed time :  "+str(totalTime)
	PrintTime("green", message)
	PrintTime("console", "\tEND")
	print("\n")


	exit()



# this function will go through the pileup dictionnary and replace sets with lists
# this is because to successfully dump the pileup as a msppack object, sets are not
# supported and should be replaced with basic lists
def RemoveSets(d):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			d[chrom][position]['A'][2] = list(composition['A'][2])
			d[chrom][position]['C'][2] = list(composition['C'][2])
			d[chrom][position]['G'][2] = list(composition['G'][2])
			d[chrom][position]['T'][2] = list(composition['T'][2])
			
			for key, value in composition['in'].items():
				d[chrom][position]['in'][key][2] = list(value[2])
			for key, value in composition['del'].items():
				d[chrom][position]['del'][key][2] = list(value[2])

	return d



# this function will print the counts in the pileup at a specific genomic location
# it will prints the genomic location, the A,C,G,T,ins,del forward and reverse counts
# as well as the reference base at this location and the corresponding depth
def PrintPileup(pileup, index):
	index = index.split(":")
	try:
		composition = pileup[index[0]][int(index[1])]
	except:
		print("\n")
		PrintTime('error', "\tPosition "+":".join(index)+" not in the pileup")
		return None

	PrintTime('console', "\tPosition "+":".join(index)+" Breakdown :")
	print("\n")

	print(" ref : "+composition['ref']+"\n")

	print("  A  : "+str(composition['A'][0]+composition['A'][1]))
	print("  C  : "+str(composition['C'][0]+composition['C'][1]))
	print("  G  : "+str(composition['G'][0]+composition['G'][1]))
	print("  T  : "+str(composition['T'][0]+composition['T'][1]))
		
	n_ins_pos = 0
	n_ins_neg = 0
	for value in composition['in'].values():
		n_ins_pos += value[0]
		n_ins_neg += value[1]
	
	n_del_pos = 0
	n_del_neg = 0
	for value in composition['del'].values():
		n_del_pos += value[0]
		n_del_neg += value[1]

	print("  +  : "+str(n_ins_pos+n_ins_neg))
	print("  -  : "+str(n_del_pos+n_del_neg))
	print("  #  : "+str(composition['depth'])+"\n")

	print(' base_error_probability : '+str(composition['base_error_probability']))
	print("   HP   : "+str(composition['HP'])+"\n")

	if composition['alt'] == "in":
		AO = n_ins_pos+n_ins_neg
	elif composition['alt'] == "del":
		AO = n_del_pos+n_del_neg
	else:
		AO = composition[composition["alt"]][0]+composition[composition["alt"]][1]
	
	print(' q-value : '+str(round(composition['q-value'], 4)))
	print(" ALT1    : "+composition['alt'])
	print(' AF1     : '+str(round(float(AO)/composition["depth"], 4)))
	print(' n_umi1  : '+str(composition['alt_concordant']))
	print(' SB1     : '+str(round(composition['SB'], 3))+"\n")


	if composition['alt2'] != None:
		if composition['alt2'] == "in":
			AO = n_ins_pos+n_ins_neg
		elif composition['alt2'] == "del":
			AO = n_del_pos+n_del_neg
		else:
			AO = composition[composition["alt2"]][0]+composition[composition["alt2"]][1]
		
		print(' q-value2: '+str(round(composition['q-value2'], 4)))
		print(" ALT2    : "+composition['alt2'])
		print(' AF2     : '+str(round(float(AO)/composition["depth"], 4)))
		print(' n_umi2  : '+str(composition['alt_concordant2']))
		print(' SB2     : '+str(round(composition['SB2'], 3))+"\n")



	if composition['alt3'] != None:
		if composition['alt3'] == "in":
			AO = n_ins_pos+n_ins_neg
		elif composition['alt3'] == "del":
			AO = n_del_pos+n_del_neg
		else:
			AO = composition[composition["alt3"]][0]+composition[composition["alt3"]][1]
		
		print(' q-value3: '+str(round(composition['q-value3'], 4)))
		print(" ALT3    : "+composition['alt3'])
		print(' AF3     : '+str(round(float(AO)/composition["depth"], 4)))
		print(' n_umi3  : '+str(composition['alt_concordant3']))
		print(' SB3     : '+str(round(composition['SB3'], 3))+"\n")


	print("\n")



# this function will take a position as an integer and split it into
# three digits separated by a space if length > 4. For example : 
# 1245 => 1245 and 125487 => 125 487.
def Format(x):
	y = str(x)
	length = len(y)
	z = ""

	if length > 4:
		res = length%3
		while length > res:
			new = y[-3:]
			z = new+" "+z
			length -= 3
			y = y[:length]

		z = y[:res]+" "+z
	else:
		z = y

	return z



# this function will print the details about a certain variant given under one of these
# three formats : chrX:1215458A>T, chrX:1215458>del or chrX:1215458A>ins.
# it will print the genomic location, the A,C,G,T,ins,del, depth, total counts, the 
# reference and alt base, AF, HP length, qNoise, SB and the number of alt concordant UMI.
def PrintVariant(pileup, variant):
	variant = variant.split(":")
	chrom = variant[0]
	ref = variant[1][variant[1].index('>')-1] if "del" not in variant[1] and "ins" not in variant[1] else ""
	alt = variant[1][variant[1].index('>')+1:]
	position = int(variant[1].replace(ref+'>'+alt, ''))

	try:
		composition = pileup[chrom][position]
	except:
		try:
			composition = pileup[chrom][position+1]
		except:
			print("\n")
			PrintTime('error', "\tVariant "+":".join(variant)+" not in the pileup")
			return None

	print("\n")
	# PrintTime('console', "\tVariant "+chrom+" : "+Format(position)+" "+ref+" > "+alt+" Breakdown :")
	PrintTime('console', "\tVariant "+":".join(variant)+" Breakdown :")
	print("\n")

	
	#############################################################
	###############   if variant is substitution ################
	#############################################################


	if len(alt) == 1:
		print(" REF : "+composition['ref'])
		print(" ALT : "+alt+"\n")

		print("  A  : "+str(composition['A'][0]+composition['A'][1]))
		print("  C  : "+str(composition['C'][0]+composition['C'][1]))
		print("  G  : "+str(composition['G'][0]+composition['G'][1]))
		print("  T  : "+str(composition['T'][0]+composition['T'][1]))
		
		n_ins_pos = 0
		n_ins_neg = 0
		for value in composition['in'].values():
			n_ins_pos += value[0]
			n_ins_neg += value[1]
		
		n_del_pos = 0
		n_del_neg = 0
		for value in composition['del'].values():
			n_del_pos += value[0]
			n_del_neg += value[1]

		print("  +  : "+str(n_ins_pos+n_ins_neg))
		print("  -  : "+str(n_del_pos+n_del_neg))
		print("  #  : "+str(composition['depth'])+"\n")

		
		if composition['alt'] == alt:
			AO = composition[composition["alt"]][0]+composition[composition["alt"]][1]
	
			print('   AF   : '+str(round(float(AO)/composition["depth"], 4)))
			print(' q-value: '+str(round(composition['q-value'], 4)))
			print('  n_umi : '+str(composition['alt_concordant']))
			print('   SB   : '+str(round(composition['SB'], 3)))

		elif composition['alt2'] == alt:
			AO = composition[composition["alt2"]][0]+composition[composition["alt2"]][1]
		
			print('   AF   : '+str(round(float(AO)/composition["depth"], 4)))
			print(' q-value: '+str(round(composition['q-value2'], 4)))
			print('  n_umi : '+str(composition['alt_concordant2']))
			print('   SB   : '+str(round(composition['SB2'], 3)))

		else:
			AO = composition[composition["alt3"]][0]+composition[composition["alt3"]][1]
		
			print('   AF   : '+str(round(float(AO)/composition["depth"], 4)))
			print(' q-value: '+str(round(composition['q-value3'], 4)))
			print('  n_umi : '+str(composition['alt_concordant3']))
			print('   SB   : '+str(round(composition['SB3'], 3)))
		
		print("   HP   : "+str(composition['HP'])+"\n")


	else:

		#############################################################
		#################   if variant is deletion ##################
		#############################################################


		if alt == "del":

			try:
				composition = pileup[chrom][position+1]
			except:
				print("\n")
				PrintTime('error', "\tVariant "+":".join(variant)+" not in the pileup")
				return None

			print(" REF : "+composition['ref'])
			print(" ALT : - \n")

			print("  A  : "+str(composition['A'][0]+composition['A'][1]))
			print("  C  : "+str(composition['C'][0]+composition['C'][1]))
			print("  G  : "+str(composition['G'][0]+composition['G'][1]))
			print("  T  : "+str(composition['T'][0]+composition['T'][1]))
			
			n_ins_pos = 0
			n_ins_neg = 0
			for value in composition['in'].values():
				n_ins_pos += value[0]
				n_ins_neg += value[1]
			
			n_del_pos = 0
			n_del_neg = 0
			for value in composition['del'].values():
				n_del_pos += value[0]
				n_del_neg += value[1]

			print("  +  : "+str(n_ins_pos+n_ins_neg))
			print("  -  : "+str(n_del_pos+n_del_neg))
			print("  #  : "+str(composition['depth'])+"\n")

			AO = n_del_pos+n_del_neg
			print('   AF   : '+str(round(float(AO)/composition["depth"], 4)))

			if composition['alt'] == alt:
				print(' q-value: '+str(round(composition['q-value'], 4)))
				print('  n_umi : '+str(composition['alt_concordant']))
				print('   SB   : '+str(round(composition['SB'], 3)))

			elif composition['alt2'] == alt:
				print(' q-value: '+str(round(composition['q-value2'], 4)))
				print('  n_umi : '+str(composition['alt_concordant2']))
				print('   SB   : '+str(round(composition['SB2'], 3)))

			else:
				print(' q-value: '+str(round(composition['q-value3'], 4)))
				print('  n_umi : '+str(composition['alt_concordant3']))
				print('   SB   : '+str(round(composition['SB3'], 3)))
			
			print("   HP   : "+str(composition['HP'])+"\n")



		else:

			#############################################################
			#################  if variant is insertion ##################
			#############################################################
			
			alt = "in"

			try:
				composition1 = pileup[chrom][position+1]
			except:
				print("\n")
				PrintTime('error', "\tVariant "+":".join(variant)+" not in the pileup")
				return None

			print(" REF : "+composition['ref'])
			print(" ALT : + \n")

			print("  A  : "+str(composition['A'][0]+composition['A'][1]))
			print("  C  : "+str(composition['C'][0]+composition['C'][1]))
			print("  G  : "+str(composition['G'][0]+composition['G'][1]))
			print("  T  : "+str(composition['T'][0]+composition['T'][1]))
			
			n_ins_pos = 0
			n_ins_neg = 0
			for value in composition1['in'].values():
				n_ins_pos += value[0]
				n_ins_neg += value[1]
			
			n_del_pos = 0
			n_del_neg = 0
			for value in composition['del'].values():
				n_del_pos += value[0]
				n_del_neg += value[1]

			print("  +  : "+str(n_ins_pos+n_ins_neg))
			print("  -  : "+str(n_del_pos+n_del_neg))
			print("  #  : "+str(composition['depth'])+"\n")

			AO = n_ins_pos+n_ins_neg
			print('   AF   : '+str(round(float(AO)/composition["depth"], 4)))

			if composition1['alt'] == alt:
				print(' q-value: '+str(round(composition1['q-value'], 4)))
				print('  n_umi : '+str(composition1['alt_concordant']))
				print('   SB   : '+str(round(composition1['SB'], 3)))

			elif composition1['alt2'] == alt:
				print(' q-value: '+str(round(composition1['q-value'], 4)))
				print('  n_umi : '+str(composition1['alt_concordant2']))
				print('   SB   : '+str(round(composition1['SB2'], 3)))

			else:
				print(' q-value: '+str(round(composition1['q-value'], 4)))
				print('  n_umi : '+str(composition1['alt_concordant3']))
				print('   SB   : '+str(round(composition1['SB3'], 3)))
			
			print("   HP   : "+str(composition1['HP'])+"\n")


	print("\n")



# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculates the 
# depth. the depth = nA+ + nA- + nC+ + nC- + nG+ + nG- + nT+ + nT- + nDel+ + nDel- 
# insertion counts are not taken into account in depth calculation
def AddDepths(d):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			depth =  composition['A'][0]+composition['A'][1]
			depth += composition['C'][0]+composition['C'][1]
			depth += composition['G'][0]+composition['G'][1]
			depth += composition['T'][0]+composition['T'][1]

			n_ins = 0
			for value in composition['in'].values():
				n_ins += value[0]+value[1]
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# insertions shouldn't be taken into account when calculating depth ???
			depth += n_ins
			depth += n_del

			d[chrom][position]['depth'] = depth

	return d


# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculate use the
# position depth and the total base quality scores to estimate 
# the noise level at the position
def EstimateNoise(d):
	for chrom, infos in d.items():
		for position, composition in infos.items():
			# get total base quality scores at this position
			qScore = d[chrom][position]['base_error_probability']
			
			# get depth
			depth = d[chrom][position]['depth']
			
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# substract deletion from depth because deletions are not taken into account
			# in the total qscore since deletions don't have qscore
			depth -= n_del
			# divide by the depth to get mean qscore of the position
			mean_qscore = math.floor(float(qScore)/depth) if depth > 0 else 0
			d[chrom][position]['qScore'] = mean_qscore

			# qScore = -10.log(base_error_probability)
			# use this formula to calculate base_error probability
			base_error_probability = 10**(float(mean_qscore)/-10)
			# add the estimated base_error_probability to the position in the pileup
			d[chrom][position]['base_error_probability'] = round(base_error_probability, 6)

	return d



# this function takes the pileup dictionnary as an argument and the refernce genome
# for each chrom and at each position, it will retrieve the reference base in the given
# genome and add it to the pileup dictionnary
def AddRefBases(d, f):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			ref = str(f[chrom][position-1])
			d[chrom][position]['ref'] = ref.upper()

	return d


# this function takes 3 arguments : the quality string of the read, 
# the quals chracters string and the minimum read quality
# if the mean quality of the read is < minimum read quality, the
# function will return False (meaning the read is invalid) 
def QualIsValid(qual, quals, min_read_qual):
	total = 0
	l_read = len(qual)
	threshold = min_read_qual*l_read

	step = 0
	for q in qual:
		total += quals[q]
		step += 1
		if step == 10:
			if total >= threshold:
				return True
			step=0


	return False



# this function will check if the read is valid
# a valid orphan must not be an orphan, must be aligned to chromosome
# at a certain position, must have an UMI of length 12 (customizable),
# must have a read quality > min_read_quality and a mapping quality 
# different than 255. if all these conditions are met, the read is valid
# and the function returns True
def ReadIsValid(flag, chrom, pos, umi, cigar, mapq, min_mapping_qual, qual, quals, min_read_qual):

	orphan = not bool(int("{0:b}".format(int(flag))[-2]))

	# test if orphan
	if orphan:
		return False


	try:
		# test chromosome format
		# test = chrom.index('chr')

		# test pos format
		test = 25/int(pos)

		# test umi length
		# lUMI = len(umi)
		# test = 25/(lUMI-1)
		# test = 25/(lUMI-2)
		# test = "xx12xx".index(str(len(umi)))
		
		# test cigar format
		test = 25/(1-len(cigar))

		# test read quality
		if not QualIsValid(qual, quals, min_read_qual):
			return False

		# test mapping quality
		if mapq == 255 or mapq < min_mapping_qual:
			return False



		return True

	except:

		return False


# this function will calculate the total number of keys in the given dictionnary
# and return it as a value
def GetPileupLength(pileup):
	total = 0
	
	for chrom , infos in pileup.items():
		for position, composition in infos.items():
			total += 1

	return total


# this function will calculate the total number of variants in the pileup dictionnary
# and return it as a value
def GetTotalVariants(pileup):
	total = 0
	
	for chrom , infos in pileup.items():
		for position, composition in infos.items():
			total += 1
			if composition["alt2"] != None:
				total += 1
			if composition["alt3"] != None:
				total += 1

	return total


# this function will return the number of lines in a file
def GetTotalLines(filename):
	with open(filename) as f:
		for i, l in enumerate(f):
			pass
	return i + 1


# this function takes the pileup dictionnary as an argument and the refernce genome
# for each chrom and at each position, it will calculate the length of the homopolymer 
# and add it to the pileup dictionnary
def AddHomoPolymers(d, f):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			ref = str(f[chrom][position-1])

			hp = 1
			# check hp left - max - 20
			for i in range(2, 22):
				x = str(f[chrom][position-i])
				if x == ref:
					hp += 1
				else:
					break

			# check hp right - max - 20
			for i in range(0, 20):
				x = str(f[chrom][position+i])
				if x == ref:
					hp += 1
				else:
					break


			d[chrom][position]['HP'] = hp

	return d



# this function takes the pileup dictionnary as an argument
# it will sort the dictionnary by chromosome first, and by position
# second and return the sorted dictionnary.
def SortPileup(pileup):

	addCHR = False
	for chrom in pileup.keys():
		if "chr" in chrom:
			addCHR = True
			break

	num_chroms = []
	alpha_chroms = []
	new_pileup = OrderedDict()
	final_pileup = OrderedDict()

	for chrom in pileup.keys():
		try:
			num_chroms.append(int(chrom.replace('chr', '')))
		except:
			alpha_chroms.append(chrom.replace('chr', ''))

	num_chroms = sorted(num_chroms)
	alpha_chroms = sorted(alpha_chroms)
	sorted_chroms = num_chroms + alpha_chroms
	
	for chrom in sorted_chroms:
		chrom = 'chr'+str(chrom) if addCHR else str(chrom)
		poss = pileup[chrom].keys()
		sorted_poss = sorted(poss)
		new_pileup[chrom] = OrderedDict()
		for pos in sorted_poss:
			new_pileup[chrom][pos] = pileup[chrom][pos]

	return new_pileup



# this function aims to create a distinct copy of the given dict 
def CopyPileup(pileup):
	new_pileup = OrderedDict()
	for chrom in pileup.keys():
		new_pileup[chrom] = OrderedDict()
		for position in pileup[chrom].keys():
			new_pileup[chrom][position] = pileup[chrom][position]

	return new_pileup



# this function will call saltools to convert the BAM file into a SAM file
def BAMtoSAM(bam):
	sam = bam.replace('.bam', '.sam')
	samFile = open(sam, "w")
	samFile.close()
	pysam.view('-o', sam, bam, save_stdout=sam)
	return sam




# this function will give a variant a confidence level based
# on its q-value, alt_concordant_umi, strand bias and hp length.
def ComputeConfidence(qval, alt_discordant, alt_concordant, hp, Cp, Cm, Vp, Vm, variant_mean_qscore, position_mean_qscore, position_stdev):

	sb = CalculateStrandBias(Cp, Cm, Vp, Vm, "default")
	confs = ['low', 'average', 'high', 'strong', 'certain']
	qScoreWeight = 1.0
	UMIweight = 1.0
	SBweight = 1.0
	HPweight = 1.0
	qvalWeight = 1.0
	levels = []


	# check q-value
	if qval == 0:
		score = 5
	elif qval < 0.00005:
		score = 4
	elif qval < 0.0005:
		score = 3
	elif qval < 0.005:
		score = 2
	else:
		score = 1
	levels.append(score*qvalWeight)



	# check discordant/concordant ratio
	# checking the ratio can give false negatives since the probability of creating a discordant UMI
	# by chance is much higher than that of creating a concordant UMI.
	# number of discordant UMIs should not be taken into account for confidence calculation. 
	# if alt_dis == 0:
	# 	score = 5
	# else:
	if alt_concordant > 30:
		score = 5
	elif alt_concordant > 20:
		score = 4
	elif alt_concordant > 15:
		score = 3
	elif alt_concordant > 10:
		score = 2
	else:
		score = 1
	levels.append(score*UMIweight)


	# check SB
	if sb < 0.5:
		score = 5
	elif sb < 0.60:
		score = 4
	elif sb < 0.70:
		score = 3
	elif sb < 0.80:
		score = 2
	else:
		score = 1
	levels.append(score*SBweight)

	
	# check HP
	if hp < 3:
		score = 5
	elif hp == 3:
		score = 4
	elif hp == 4:
		score = 3
	elif hp == 5:
		score = 2
	else:
		score = 1
	levels.append(score*HPweight)


	# check qscore
	# if it's not deletion
	if variant_mean_qscore != "-":
		setPoints = [position_mean_qscore-(3*position_stdev), position_mean_qscore-(2*position_stdev), position_mean_qscore-position_stdev, position_mean_qscore]
		if variant_mean_qscore >= setPoints[3]:
			score = 5
		elif variant_mean_qscore >= setPoints[2]:
			score = 4
		elif variant_mean_qscore >= setPoints[1]:
			score = 3
		elif variant_mean_qscore >= setPoints[0]:
			score = 2
		else:
			score = 1
		levels.append(score*qScoreWeight)


	# print(levels)	
	conf = int(math.floor(float(sum(levels))/(qvalWeight+SBweight+HPweight+UMIweight))) if variant_mean_qscore == "-" else int(math.floor(float(sum(levels))/(qvalWeight+SBweight+HPweight+UMIweight+qScoreWeight)))
	return [conf, confs[conf-1]]



# this function will calculate the SB of the variant depending on the method the user selected
def CalculateStrandBias(Cp, Cm, Vp, Vm, method):
	if method == "default":
		return abs(((float(Vp)/(Cp+Vp))-(float(Vm)/(Cm+Vm)))/((float(Vp+Vm))/(Cp+Vp+Cm+Vm)))
	else:
		return float(max([Vp*Cm,Vm*Cp]) / (Vp*Cm + Vm*Cp))











def CorrectDeletions(variants):

	pos_to_skip = OrderedDict()

	for chrom in variants.keys():

		for position, info in variants[chrom].items():

			try:
				line_info = info['line']
			except:
				line_info = info[0]



			del_seq = line_info.split('\t')[3]
			del_length = len(del_seq)-1
			variantHGVS = chrom.split('|')[0]+":"+str(position-1)+del_seq+">"+del_seq[0]
			
			if "DEL" in line_info:
				if variantHGVS not in pos_to_skip.keys():
					for k in range(position, position+del_length-1):
						nextDelHGVS = chrom.split('|')[0]+":"+str(k)+del_seq[k-position+1:]+">"+del_seq[k-position+1]
						nextDelKey = chrom+"-"+str(k+1)
						pos_to_skip[nextDelHGVS] = nextDelKey



	for variantHGVS, variantKey in pos_to_skip.items():

		variantKey = variantKey.split("-")

		try:
			line_info = variants[variantKey[0]][int(variantKey[1])]['line']
		except:
			try:
				line_info = variants[variantKey[0]][int(variantKey[1])][0]
			except:
				pass



		if "DEL" in line_info and variantHGVS in line_info and "WHITE_LIST" not in line_info:
			try:
				del variants[variantKey[0]][int(variantKey[1])]
			except:
				pass



	return variants






# this fuction will adapt to the float size in order to 
# round number to the smallest decimal without getting
# a 0.à as result
def AdaptiveRound(x):

	i = 3
	while round(x, i) == 0.0:
		i+= 1

	return round(x, i)





def Add_Depth_Noise_Ref_HP(d, f):

	currentLine = 1.0
	lastProgress = 0.0
	totalLines = GetPileupLength(d)

	for chrom , infos in d.items():
		
		for position, composition in infos.items():

			lastProgress = PrintProgress(currentLine, totalLines, lastProgress)
			currentLine += 1

			# add ref
			ref = str(f[chrom][position-1])
			d[chrom][position]['ref'] = ref.upper()


			# add homopolymer info
			hp = 1
			# check hp left - max - 20
			for i in range(2, 22):
				x = str(f[chrom][position-i])
				if x == ref:
					hp += 1
				else:
					break

			# check hp right - max - 20
			for i in range(0, 20):
				x = str(f[chrom][position+i])
				if x == ref:
					hp += 1
				else:
					break


			d[chrom][position]['HP'] = hp


			# add depth
			depth =  composition['A'][0]+composition['A'][1]
			depth += composition['C'][0]+composition['C'][1]
			depth += composition['G'][0]+composition['G'][1]
			depth += composition['T'][0]+composition['T'][1]

			n_ins = 0
			for value in composition['in'].values():
				if isinstance(value, list) :
					n_ins += value[0]+value[1]
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# insertions shouldn't be taken into account when calculating depth ???
			depth += n_ins
			depth += n_del

			d[chrom][position]['depth'] = depth


			# add noise estimation
			# get total base quality scores at this position
			# qScore = d[chrom][position]['base_error_probability']
			try:
				# mean_qscore = statistics.mean(d[chrom][position]['base_error_probability'])
				# stdev_qscore = statistics.stdev(d[chrom][position]['base_error_probability'])
				mean_qscore = sum(d[chrom][position]['base_error_probability'])/len(d[chrom][position]['base_error_probability'])
				stdev_qscore = math.sqrt(sum([(x - mean_qscore)**2 for x in d[chrom][position]['base_error_probability']])/len(d[chrom][position]['base_error_probability']))
			except:
				mean_qscore = 0
				stdev_qscore = 0


			# get depth
			depth = d[chrom][position]['depth']
			
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# substract deletion from depth because deletions are not taken into account
			# in the total qscore since deletions don't have qscores
			depth -= n_del
			# divide by the depth to get mean qscore of the position
			# mean_qscore = math.floor(float(qScore)/depth) if depth > 0 else 0
			d[chrom][position]['qScore'] = mean_qscore

			# qScore = -10.log(base_error_probability)
			# use this formula to calculate base_error probability
			base_error_probability = 10**(float(math.floor(mean_qscore))/-10)
			# add the estimated base_error_probability to the position in the pileup
			d[chrom][position]['base_error_probability'] = round(base_error_probability, 6)
			d[chrom][position]['qScore_stdev'] = round(stdev_qscore, 6)



			# add noise estimation per base
			# get total base quality scores at this position
			for base in "ACGT":
				base_qScore = d[chrom][position][base][3]
			
				# get base depth
				base_depth = d[chrom][position][base][0] + d[chrom][position][base][1]
				
				# divide by the depth to get mean qscore of the position
				mean_base_qscore = math.floor(float(base_qScore)/base_depth) if base_depth > 0 else 0
				d[chrom][position][base][3] = mean_base_qscore


			# add noise estimation for insertions
			# get total base quality scores at this position
			insertion_qScore = d[chrom][position]['in']['qScore'] if d[chrom][position]['in'] else 0
			
			# get base depth
			insertion_depth = 0
			for value in composition['in'].values():
				if isinstance(value, list):
					insertion_depth += value[0]+value[1]
				
			# divide by the depth to get mean qscore of the position
			mean_insertion_qscore = math.floor(float(insertion_qScore)/insertion_depth) if insertion_depth > 0 else 0
			if mean_insertion_qscore > 0:
				d[chrom][position]['in']['qScore'] = mean_insertion_qscore
			
	return d





# function to concatenate dicts containing pileups
# a simple update does not work : if the same chromosomes
# are in two or more dicts, the content will be wiped
# and only the content of the last dict will be kept
def ConcatenatePileups(pileups):
	pileup = {}
	for p in pileups:
		for chrom in p.keys():
			for position, composition in p[chrom].items():
				try:
					pileup[chrom][position] = composition
				except:
					pileup[chrom] = {position: composition}
	return pileup








def PrintProgramName():
	print(
"""
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||| ||||||| |||  ||||||  |||       ||||||||||||  |||||||  |||       ||||       |||||    ||||       ||| ||||||||||||||||||
||||||||| ||||||| ||| | |||| | |||||| |||||||||||||||  |||||||  ||| ||||| |||| ||||| |||| |||| ||| ||||| ||| ||||||||||||||||||
||||||||| ||||||| ||| || || || |||||| |||||||||||||||  |||||||  ||| ||||| |||| ||||| ||| ||||||||| ||||| ||| ||||||||||||||||||
||||||||| ||||||| ||| |||  ||| |||||| ||||||      ||||  |||||  ||||       ||||       ||| |||||||||       ||| ||||||||||||||||||
||||||||| ||||||| ||| |||||||| |||||| |||||||||||||||||  |||  ||||| ||||| |||| ||  ||||| ||||||||| ||||| ||| ||||||||||||||||||
|||||||||  |||||  ||| |||||||| |||||| ||||||||||||||||||  |  |||||| ||||| |||| |||  ||||| |||| ||| ||||| ||| ||||||||||||||||||
||||||||||       |||| |||||||| |||       ||||||||||||||||   ||||||| ||||| |||| ||||  |||||    |||| ||||| |||       ||||||||||||
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||V.S||||
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
""")






# this function will return a dictionnary with all UMIS and their indexes
def GetUMIS(SAM):

	# value = set()

	# for line in open(SAM):

	# 	line = line.split('\t')
		
	# 	try:
	# 		value.add(line[0].split('_')[-1])

	# 	except:
	# 		continue

	
	# c = 0
	# final = {}

	# for el in value:
	# 	final[el] = c
	# 	c+=1

	# return final
	return 1






















def ParseList(LIST):
	if LIST != "None":
		l = set()
		k = set()

		for line in open(LIST):
			valid = True
			line = line.replace('g.', '').strip().upper()
			match = re.search("^CHR([0-9]+|X|Y):([0-9]*)([ACGT]+)>([ACGT]+)$", line)
			if match is not None:
				k.add("chr"+match.group(1)+":"+match.group(2))
				if len(match.group(3)) == len(match.group(4)) and len(match.group(3)) == 1:
					l.add("chr"+match.group(1)+":"+match.group(2)+match.group(3)+">"+match.group(4))
				elif len(match.group(3)) > len(match.group(4)):
					if match.group(3)[:1] == match.group(4):
						l.add("chr"+match.group(1)+":"+match.group(2)+">del")
					else:
						valid = False
				elif len(match.group(3)) < len(match.group(4)):
					if match.group(3) == match.group(4)[:1]:
						l.add("chr"+match.group(1)+":"+match.group(2)+">ins")
					else:
						valid = False
				else:
					valid = False
			else:
				valid = False

			if not valid:
				PrintTime("warning", "\t\tVariant "+line.replace('CHR', 'chr')+" has an invalid format ! Skipped...")


		LIST = {"pos": list(k), "var": list(l)}
	else:
		LIST = {"pos": [], "var": []}


	return LIST



def ParseLists(BLACK_LIST, WHITE_LIST):

	BLACK_LIST = ParseList(BLACK_LIST)
	WHITE_LIST = ParseList(WHITE_LIST)


	if len(BLACK_LIST['pos']) > 0 and len(WHITE_LIST['pos']) > 0:
		
		to_remove_pos =[]
		to_remove_black_var =[]
		to_remove_white_var =[]

		for x in WHITE_LIST['pos']:
			if x in BLACK_LIST['pos']:
				PrintTime("warning", "\t\tPosition "+x+" cannot be black and white listed at the same time ! Skipped...")
				to_remove_pos.append(x)
				
				for y in BLACK_LIST['var']:
					match = re.search("^chr([0-9]+|X|Y):([0-9]*)([ACGT]+)>([ACGT]+)$", y)
					if match != None:
						if x == "chr"+match.group(1)+":"+match.group(2):
							to_remove_black_var.append(y)
					else:
						if x == y.replace('>del', '').replace('>ins', ''):
							to_remove_black_var.append(y)
				
				for z in WHITE_LIST['var']:
					match = re.search("^chr([0-9]+|X|Y):([0-9]*)([ACGT]+)>([ACGT]+)$", z)
					if match != None:
						if x == "chr"+match.group(1)+":"+match.group(2):
							to_remove_white_var.append(z)
					else:
						if x == z.replace('>del', '').replace('>ins', ''):
							to_remove_white_var.append(z)



		for x in to_remove_pos:
			BLACK_LIST['pos'].remove(x)
			WHITE_LIST['pos'].remove(x)

		for x in to_remove_black_var:
			BLACK_LIST['var'].remove(x)

		for x in to_remove_white_var:
			WHITE_LIST['var'].remove(x)

		# print(BLACK_LIST['pos'])
		# print(BLACK_LIST['var'])
		# print(WHITE_LIST['pos'])
		# print(WHITE_LIST['var'])

	PrintTime("green", "\t\t"+str(len(WHITE_LIST['var']))+" variants are white listed")
	PrintTime("green", "\t\t"+str(len(BLACK_LIST['var']))+" variants are black listed")




	return(BLACK_LIST['var'], WHITE_LIST)


# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY MATCHED/MISMATCHED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -READNAME          : (STR)  NAME/ID OF THE READ
# 		  -UMI               : (STR)  UMI SEQUENCE OF THE READ
# 		  -CHROM             : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -START             : (INT)  THE START POSITION OF THE READ
# 		  -SEQ               : (STR)  THE SEQUENCE OF THE READ
# 		  -STRAND            : (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CURSOR_POS        : (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ        : (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
# 		  -MAXX              : (INT)  THE LENGTH OF THE CIGAR 'M' ELEMENT
# 		  -QUAL              : (STR)  THE QUALITY STRING OF THE READ
# 		  -QUALS             : (DICT) A DICTIONNARY FOR THE CONVERSION OF THE QUALITIES FROM ASCII TO INT
# 		  -MIN_BASE_QUALITY  : (INT)  MINIMUM QUALITY SCORE OF THE BASE FOR IT TO ADDED TO THE PILEUP DICTIONNARY
#
# VALUE : -POSITION          : (INT)  THE FINAL POSITION THAT'S BEEN PARSED IN THE READ
#		  -CURSOR_POS        : (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ        : (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ


def AddMatches(pileup, readName, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY):

	# for each position between limits
	for position in range(start+cursor_pos, start+cursor_pos+maxx):

		# try because base position in read could not be in BED file
		# therefore, the position could not be in the PILEUP dictionnary
		# if position not in BED, skip the read
		try:	

			# get base, base_qual and base_qscore
			base = seq[cursor_seq]
			baseQual = qual[cursor_seq]
			base_qscore = quals[baseQual]

				
			# check if the quality of the base >= minimum base quality
			if base_qscore >= MIN_BASE_QUALITY:
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position][base][strand] += 1
				# add the umi to the corresponding set specific to the base in the PILEUP dictionnary
				pileup[chrom][position][base][2].append(umi)
				# increment the qScore of the base at this position
				pileup[chrom][position][base][3] += base_qscore
				# add the readName to the corresponding list specific to the base in the PILEUP dictionnary
				pileup[chrom][position][base][4].append(readName)
				# increment the qScore of this position
				pileup[chrom][position]['base_error_probability'].append(base_qscore)

			

		# if position not in BED, skip the read
		except:
			pass	

		# increment the cursor_seq to allow advancing in the read sequence
		cursor_seq += 1


	# increment the cursor_pos with the length of the cigar element when done adding it
	cursor_pos += maxx


	# return position, cursor_seq and cursor_pos to continue from where we left off
	return [position, cursor_seq, cursor_pos]







def add_depth_noise_ref_hp(d, f):

	for chrom , infos in d.items():

		for position, composition in infos.items():
			# add depth
			ref = f[chrom][position-1]
			d[chrom][position]['ref'] = ref.upper()


			# add homopolymer info
			hp = 1
			# check hp left - max - 20
			for i in range(2, 22):
				x = f[chrom][position-i]
				if x == ref:
					hp += 1
				else:
					break

			# check hp right - max - 20
			for i in range(0, 20):
				x = f[chrom][position+i]
				if x == ref:
					hp += 1
				else:
					break


			d[chrom][position]['HP'] = hp


			# add depth
			depth =  composition['A'][0]+composition['A'][1]
			depth += composition['C'][0]+composition['C'][1]
			depth += composition['G'][0]+composition['G'][1]
			depth += composition['T'][0]+composition['T'][1]

			n_ins = 0
			for value in composition['in'].values():
				n_ins += value[0]+value[1]
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# insertions shouldn't be taken into account when calculating depth ???
			depth += n_ins
			depth += n_del

			d[chrom][position]['depth'] = depth


			# add noise estimation
			# get total base quality scores at this position
			qScore = d[chrom][position]['base_error_probability']
			
			# get depth
			depth = d[chrom][position]['depth']
			
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# substract deletion from depth because deletions are not taken into account
			# in the total qscore since deletions don't have qscore
			depth -= n_del
			# divide by the depth to get mean qscore of the position
			mean_qscore = math.floor(float(qScore)/depth) if depth > 0 else 0
			d[chrom][position]['qScore'] = mean_qscore

			# qScore = -10.log(base_error_probability)
			# use this formula to calculate base_error probability
			base_error_probability = 10**(float(mean_qscore)/-10)
			# add the estimated base_error_probability to the position in the pileup
			d[chrom][position]['base_error_probability'] = round(base_error_probability, 6)

			
			return d






# THIS FUNCTION ALLOWS TO CONFIGURE THE PARAMETERS FROM THE COMMAND LINE
# AND PROPERLY PASS THEM TO THE CORRESPONDING FUNCTION CALLS
#
# INPUT : 
# 		  NONE
#
# VALUE : -CONFIG              : (DICT)  A DICTIONNARY THAT CONTAINS ALL NECESSARY THE PARAMETERS AND THEIR VALUES
#		



def Config():
	
	config={}

	# get versionning details from README
	README_PATH = sys.argv[0].replace('umi-varcal.py', 'README.md')

	version = open(README_PATH).readlines()[3].split(':')[1].strip()
	lastUpdate = open(README_PATH).readlines()[4].split(':')[1].strip()

	# if "=" in arguments, replace them with ' '
	argList = " ".join(sys.argv)
	sys.argv = argList.replace('=', ' ').split(' ')

	if len(sys.argv) == 1 or "--help" in sys.argv or "-h" in sys.argv:

		if "extract" in sys.argv:
			PrintHelp("extract", version, lastUpdate)
		elif "call" in sys.argv:
			PrintHelp("call", version, lastUpdate)
		else:
			PrintHelp("general", version, lastUpdate)



	# configure the extraction tool
	elif sys.argv[1] == "extract":
		if len(sys.argv) == 2:
			PrintHelp("extract", version, lastUpdate)


		### just in case script path contains '-' or '--'
		sys.argv[0] = "None"
		sys.argv.remove('extract')


		# default parameters
		config["bwa_threads"] = 1


		### required parameters
		# input       : path to the bam/sam file
		# fasta       : path to the fasta file
		# umi length  : length of the umi

		required = ['--input | -i', '--fasta | -f', '--umi_length | -l']
		for param in required:
			missing = False
			for arg in param.split(" | "):
				try:
					test = sys.argv.index(arg)
					missing = False
					break
				except:
					missing = True



			if missing:
				PrintTime('error', "\tThe parameter "+param+" is required but missing !\n\t\t\tExiting...")
				exit()
		


		print("\n")
		PrintTime("console", "\tConfiguring Parameters...")

		# differentiate the '-' in fasta of the '-' for arguments
		try:
			fastaIdx = sys.argv.index("--fasta")

			fastaIdx+=1
			sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
		except:
			try:
				fastaIdx = sys.argv.index("-f")

				fastaIdx+=1
				sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
			except:
				pass



		# differentiate the '-' in input of the '-' for arguments
		try:
			inputIdx = sys.argv.index("--input")

			inputIdx+=1
			sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
		except:
			try:
				inputIdx = sys.argv.index("-i")

				inputIdx+=1
				sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
			except:
				pass










		### verify that no arguments are empty
		params_in_config = ['input', 'fasta', 'umi_length']
		params_mini = ['i', 'f', 'l']
		params = ['--input | -i', '--fasta | -f', '--umi_length | -l']

		pointer = 1
		while pointer < len(sys.argv):
			param = sys.argv[pointer]
			try:
				value = sys.argv[pointer+1]
			except:
				if "--" in param:
					if param in ['--input', '--fasta', '--umi_length']:
						PrintTime('error', "\tThe parameter "+params[params_in_config.index(param.replace("--", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("--", "") in params_in_config:
							PrintTime('error', "\tThe parameter's "+params[params_in_config.index(param.replace("--", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				elif "-" in param and "--" not in param:
					if param in ['-i', '-f', '-l']:
						PrintTime('error', "\tThe parameter "+params[params_mini.index(param.replace("-", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("-", "") in params_mini:
							PrintTime('error', "\tThe parameter's "+params[params_mini.index(param.replace("-", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				else:
					PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
					exit()


			if "-" in value and len(value) == 2:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			if "--" in value and len(value) > 3:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			pointer += 2





		args=""
		for arg in sys.argv:
			args+=" "+arg


		args = args.replace("--", "|")
		args = args.replace("-", "|")

		args = args.split("|")
		del args[0]


		for arg in args:
			param = arg.split(" ")[0]
			value = arg.split(" ")[1]

			if param == "input" or param == "i":
				param = "input"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")

			if param == "fasta" or param == "f":
				param = "fasta"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "umi_length" or param == "l":
				param = "umi_length"
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()


			if param == "bwa_threads" or param == "t":
				param = "bwa_threads"
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()
			


		return config





	# configure the variant calling tool
	elif sys.argv[1] == "call":

		if len(sys.argv) < 3:

			PrintHelp("call", version, lastUpdate)

		
		### just in case script path contains '-' or '--'
		sys.argv[0] = "None"
		sys.argv.remove('call')


		### default values for parameters
		config["min_base_quality"]       = 10
		config["min_read_quality"]       = 20
		config["min_mapping_quality"]    = 20
		config["min_variant_umi"]        = 5
		config["min_variant_depth"]      = 5
		config["strand_bias_method"]     = "default"
		config["output"]                 = os.getcwd()
		config["pileup"]                 = "None"
		config["cores"]                  = 1
		config["default_cores"]          = True
		config["max_hp_length"]          = 7
		config["alpha"]                  = 0.05
		config["min_phase_umi"]          = 3
		config["min_phase_vaf_ratio"]    = 0.8
		config["max_phase_distance"]     = 150
		config["gvcf"]                   = False
		config["keep_pileup"]            = True
		config["ignore_monocluster_umi"] = False
		config["compute_coverage_stats"] = False
		config["black_list"]             = "None"
		config["white_list"]             = "None"
		config['version']                = version
		config['lastUpdate']             = lastUpdate


		### required parameters
		# input : path to the bam/sam file
		# fasta  : path to the fasta file
		# bed  : path to the bed file

		required = ['--input | -i', '--fasta | -f', '--bed | -b']
		for param in required:
			missing = False
			for arg in param.split(" | "):
				try:
					test = sys.argv.index(arg)
					missing = False
					break
				except:
					missing = True



			if missing:
				PrintTime('error', "\tThe parameter "+param+" is required but missing !\n\t\t\tExiting...")
				exit()
		

		# print program name
		PrintProgramName()
		PrintTime("console", "\tConfiguring Parameters...")

		# differentiate the '-' in fasta of the '-' for arguments
		try:
			fastaIdx = sys.argv.index("--fasta")

			fastaIdx+=1
			sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
		except:
			try:
				fastaIdx = sys.argv.index("-f")

				fastaIdx+=1
				sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
			except:
				pass




		# differentiate the '-' in bed of the '-' for arguments
		try:
			bedIdx = sys.argv.index("--bed")

			bedIdx+=1
			sys.argv[bedIdx] = sys.argv[bedIdx].replace("-", ":")
		except:
			try:
				bedIdx = sys.argv.index("-b")

				bedIdx+=1
				sys.argv[bedIdx] = sys.argv[bedIdx].replace("-", ":")
			except:
				pass
		



		# differentiate the '-' in input of the '-' for arguments
		try:
			inputIdx = sys.argv.index("--input")

			inputIdx+=1
			sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
		except:
			try:
				inputIdx = sys.argv.index("-i")

				inputIdx+=1
				sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
			except:
				pass





		# differentiate the '-' in output of the '-' for arguments
		try:
			outputIdx = sys.argv.index("--output")
		
			outputIdx+=1
			sys.argv[outputIdx] = sys.argv[outputIdx].replace("-", ":")
		except:
			try:
				outputIdx = sys.argv.index("-o")

				outputIdx+=1
				sys.argv[outputIdx] = sys.argv[outputIdx].replace("-", ":")
			except:
				pass



		# differentiate the '-' in pileup of the '-' for arguments
		try:
			pileupIdx = sys.argv.index("--pileup")

			pileupIdx+=1
			sys.argv[pileupIdx] = sys.argv[pileupIdx].replace("-", ":")
		except:
			try:
				pileupIdx = sys.argv.index("-p")

				pileupIdx+=1
				sys.argv[pileupIdx] = sys.argv[pileupIdx].replace("-", ":")
			except:
				pass



		# differentiate the '-' in pileup of the '-' for arguments
		try:
			blackListIdx = sys.argv.index("--black_list")

			blackListIdx+=1
			sys.argv[blackListIdx] = sys.argv[blackListIdx].replace("-", ":")
		except:
			pass



		# differentiate the '-' in pileup of the '-' for arguments
		try:
			whiteListIdx = sys.argv.index("--white_list")

			whiteListIdx+=1
			sys.argv[whiteListIdx] = sys.argv[whiteListIdx].replace("-", ":")
		except:
			pass





		### verify that no arguments are empty		
		params_in_config = ['input', 'bed', 'fasta', 'min_base_quality', 'min_read_quality', 'min_mapping_quality', 'min_variant_umi', 'min_variant_depth', 'strand_bias_method', 'max_strand_bias', 'output', 'pileup', 'cores', 'alpha', 'max_hp_length', 'gvcf', 'keep_pileup', 'black_list', 'white_list', 'min_phase_umi', 'min_phase_vaf_ratio', 'max_phase_distance', 'compute_coverage_stats', 'ignore_monocluster_umi']
		params_mini = ['i', 'b', 'f', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'o', 'p', 'c', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x']
		params = ['--input | -i', '--bed | -b', '--fasta | -f', '--min_base_quality', '--min_read_quality', '--min_mapping_quality', '--min_variant_umi', '--min_variant_depth', '--strand_bias_method', '--max_strand_bias', '--output | -o', '--pileup | -p', '--cores | -c', '--alpha' , '--max_hp_length', '--gvcf', '--keep_pileup', '--black_list', '--white_list', '--min_phase_umi', '--min_phase_vaf_ratio', '--max_phase_distance', '--compute_coverage_stats', '--ignore_monocluster_umi']

		pointer = 1
		while pointer < len(sys.argv):
			param = sys.argv[pointer]
			try:
				value = sys.argv[pointer+1]
			except:
				if "--" in param:
					if param in ['--input', '--bed', '--fasta']:
						PrintTime('error', "\tThe parameter "+params[params_in_config.index(param.replace("--", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("--", "") in params_in_config:
							PrintTime('error', "\tThe parameter's "+params[params_in_config.index(param.replace("--", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				elif "-" in param and "--" not in param:
					if param in ['-i', '-b', '-f']:
						PrintTime('error', "\tThe parameter "+params[params_mini.index(param.replace("-", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("-", "") in params_mini:
							PrintTime('error', "\tThe parameter's "+params[params_mini.index(param.replace("-", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				else:
					PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
					exit()


			if "-" in value and len(value) == 2:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			if "--" in value and len(value) > 3:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			pointer += 2





		args=""
		for arg in sys.argv:
			args+=" "+arg


		args = args.replace("--", "|")
		args = args.replace("-", "|")

		args = args.split("|")
		del args[0]

		sb_set = False

		for arg in args:
			param = arg.split(" ")[0]
			value = arg.split(" ")[1]

			if param == "input" or param == "i":
				param = "input"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")

			if param == "fasta" or param == "f":
				param = "fasta"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")

			if param == "bed" or param == "b":
				param = "bed"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "pileup" or param == "p":
				param = "pileup"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "output" or param == "o":
				param = "output"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "black_list":
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "white_list":
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "cores" or param == "c":
				param = "cores"
				try:
					config[param]=int(value)
					config["default_cores"] = False
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()


			if param == "min_base_quality":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_read_quality":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_mapping_quality":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_variant_umi":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_variant_depth":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "strand_bias_method":
				value = str(value).replace("'", '').replace('"', "")
				try:
					test = value.index('default')
					config[param]=value
				except:
					try:
						test = value.index('torrent_suite')
						config[param]=value
					except:
						PrintTime('error', "\tThe parameter --"+param+" can only be set to \"default\" or \"torrent_suite\" !\n\t\t\tExiting...")
						exit()

			if param == "max_strand_bias":
				try:
					config[param]=float(value)
					sb_set = True
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()	

			if param == "alpha":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "max_hp_length":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_phase_umi":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "max_phase_distance":
				try:
					config[param]=int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_phase_vaf_ratio":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()


			if param == "gvcf":

				value = value.lower()
				if value in ['true', 'false']:
					value = True if value == 'true' else False
					config[param]=value
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a boolean (True/False) !\n\t\t\tExiting...")
					exit()

			if param == "keep_pileup":

				value = value.lower()
				if value in ['true', 'false']:
					value = True if value == 'true' else False
					config[param]=value
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a boolean (True/False) !\n\t\t\tExiting...")
					exit()

			if param == "ignore_monocluster_umi":

				value = value.lower()
				if value in ['true', 'false']:
					value = True if value == 'true' else False
					config[param]=value
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a boolean (True/False) !\n\t\t\tExiting...")
					exit()
					
			if param == "compute_coverage_stats":

				value = value.lower()
				if value in ['true', 'false']:
					value = True if value == 'true' else False
					config[param]=value
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a boolean (True/False) !\n\t\t\tExiting...")
					exit()



		if not sb_set:
			if config["strand_bias_method"] == "default":
				config["max_strand_bias"] = 1.0
			else:
				config["max_strand_bias"] = 0.743

		return config



	else:
		PrintTime('error', "\tPlease precise the tool you want to use first!\n\t\t\tExiting...")
		exit()



# THIS FUNCTION WILL WRITE THE PILEUP DICTIONNARY IN A CSV FILE IN THE OUTPUT DIRECTORY 
#
# INPUT : 
#         -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#         -SAM               : (STR)  THE PATH OF THE SAM FILE
#         -OUTPUT            : (OUTPUT) THE DIRECTORY IN WHICH THE CSV FILE IS TO BE CREATED
#
# VALUE : NONE
#	



def PileupToCSV(pileup, SAM, OUTPUT):
	
	print("\n")
	PrintTime('console', "\tConverting PILEUP to CSV...")

	# create a csv file in the output dir with the same name as the SAM file
	csv = open(OUTPUT+"/"+SAM.replace(".sam", ".pileup.csv").split("/")[-1], "w")
	# create the header of the file
	header = "index,chr,position,A,C,G,T,in,del,total_reads,qScore,base_error_probability,nb_unique_umi,unique_umi_list"
	# write the header into the file
	csv.write(header+"\n")

	# counters to track progress
	currentPos = 1.0
	lastProgress = 0.0
	totalPos = GetPileupLength(pileup)


	# loop through the PILEUP dictionnary
	# for each chromosome
	for chrom , infos in pileup.items():
		# for each position | composition = counts
		for position, composition in infos.items():

			# function that displays progress efficiently
			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			# create a set that will contain unique umis
			unique_umis = set()
			
			# get n insertions at this location for the + and the - strands
			# update the unique_umis set with the insertions umis
			n_ins_pos = 0
			n_ins_neg = 0
			for value in composition['in'].values():
				n_ins_pos += value[0]
				n_ins_neg += value[1]
				unique_umis.update(value[2])
			

			# get n deletions at this location for the + and the - strands
			# update the unique_umis set with the deletions umis
			n_del_pos = 0
			n_del_neg = 0
			for value in composition['del'].values():
				n_del_pos += value[0]
				n_del_neg += value[1]
				unique_umis.update(value[2])

			# get total bases count on the + strand
			total_reads_pos = composition['A'][0]+composition['C'][0]+composition['G'][0]+composition['T'][0]
			
			# get total bases count on the - strand
			total_reads_neg = composition['A'][1]+composition['C'][1]+composition['G'][1]+composition['T'][1]
			
			# get total bases count
			total_reads = total_reads_pos+total_reads_neg
			
			# update the unique_umis set with the 4 bases umis
			for base in ['A', 'C', 'G', 'T']:
				unique_umis.update(composition[base][2])

			
			# create the line to be written in the CSV file for each position  and write it to the file
			line = [chrom+"|"+str(position), chrom, str(position), str(composition['A'][0])+"/"+str(composition['A'][1]), str(composition['C'][0])+"/"+str(composition['C'][1]), str(composition['G'][0])+"/"+str(composition['G'][1]), str(composition['T'][0])+"/"+str(composition['T'][1]),str(n_ins_pos)+"/"+str(n_ins_neg), str(n_del_pos)+"/"+str(n_del_neg), str(total_reads), str(composition['qScore']), str(composition['base_error_probability']), str(len(unique_umis)), ";".join(unique_umis)]
			line = ",".join(line)
			csv.write(line+"\n")

			# increment counter to track progress			
			currentPos += 1.0


	# close csv file
	csv.close()

	# print done
	print("\n")
	PrintTime('console', "\tDone")



