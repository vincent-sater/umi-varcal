# THIS FUNCTION WILL CREATE A COVERAGE FILE FOR THE ANALYZED SAMPLE GIVING UMI DEPTH AND READ DEPTH 
# STATISTICS FOR EACH REGION OF THE PROVIDED BED FILE
# PERCENTAGE OF THE LOCATIONS THAT HAVE A DEPTH > 0.2*AVERGAE DEPTH)
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# INPUT : -BED            	 : (STR) PATH TO THE BED FILE
# INPUT : -OUTPUT            : (STR) PATH TO THE OUTPUT FOLDER
# INPUT : -SAM            	 : (STR) SAM FILE NAME
#
# VALUE : NONE
#	

import statistics

def ComputeCoverageStats(pileup, BED, OUTPUT, SAM):
	# print("\n")
	# PrintTime('console', "\tComputing Coverage Statistics...")

	# create an empty dictionnary to store the statistic values for each region of the BED file
	coverageStats = {}

	# read the bed file line by line
	for line in open(BED):
		
		# remove trailing white spaces in line
		line = line.strip()

		# split the line using tab as delimiter
		line = line.split('\t')

		if len(line) >= 3:
			
			# check if the line containts at least chr, start, end and gene name
			region_value = line[3] if len(line) >= 4 else ""

			# Extract informations
			chr, start, end,  gene = line[0], int(line[1])+1, int(line[2]), region_value

			# Get genomic intervals
			intervals = list(range(start,(end+1)))
			
			UMIs = []
			reads = []

			region_UMIs={}
			
			for position in intervals:

				unique_umis = {}

				total_reads = 0

				try :
					composition = pileup[chr][position]

					# For each base, extract number of reads and 
					total_reads_pos = 0
					total_reads_neg = 0
					
					for base in ['A', 'C', 'G', 'T']:

						# extract number of reads on both strands
						total_reads_pos += composition[base][0]
						total_reads_neg += composition[base][1]

						# concatenate the lists
						for u in composition[base][2]:
							unique_umis[u] = None
							region_UMIs[u] = None

					total_reads = total_reads_pos + total_reads_neg

					#Unique UMIs set
					UMIs.append(len(unique_umis.keys()))
					reads.append(total_reads)

				except:
					UMIs.append(0)
					reads.append(0)


			
			coverageStats[chr+":"+str(start)+"-"+str(end)] = {
				
				"chr" :		chr,
				"start" :	start,
				"end" :		end,
				"fullUMIdepth" :	len(region_UMIs),
				"minUMIdepth" : 	min(UMIs),
				"maxUMIdepth" : 	max(UMIs),
				"medianUMIdepth" : 	statistics.median(UMIs),
				"meanUMIdepth" : 	statistics.mean(UMIs),
				"coverageFull" : 	sum(reads),
				"coverageMin" : 	min(reads),
				"coverageMax" : 	max(reads),
				"coverageMedian" : 	statistics.median(reads),
				"coverageMean" : 	statistics.mean(reads),
				"coverageSD" : 		statistics.stdev(reads),
				"region" : 			region_value
			}

	# open output file
	with open(OUTPUT+"/"+SAM.replace(".sam", "_amplicon_depth.csv").split("/")[-1], "w") as statsFile:
		# write header
		columns = ["seqnames", "start", "end", "width", "strand", "avgCoverage", "coverageSD", "coverageFULL", "coverageMIN", "coverageMAX", "coverageMedian", "coverageMean", "fullUMIdepth", "minUMIdepth", "maxUMIdepth", "medianUMIdepth", "meanUMIdepth", "amplicon", "region"]
		
		statsFile.write('"'+'";"'.join(columns)+'"\n')

		# for each region
		for region in coverageStats:
			# write values in file
			values = [coverageStats[region]["chr"], coverageStats[region]["start"],  coverageStats[region]["end"], (coverageStats[region]["end"] - coverageStats[region]["start"]), "*", coverageStats[region]["coverageMean"], coverageStats[region]["coverageSD"], coverageStats[region]["coverageFull"], coverageStats[region]["coverageMin"], coverageStats[region]["coverageMax"], coverageStats[region]["coverageMedian"], coverageStats[region]["coverageMean"], coverageStats[region]["fullUMIdepth"], coverageStats[region]["minUMIdepth"], coverageStats[region]["maxUMIdepth"], coverageStats[region]["medianUMIdepth"], coverageStats[region]["meanUMIdepth"], '"'+coverageStats[region]["region"]+'"', '"'+coverageStats[region]["region"]+'"']

			values = map(str, values)

			statsFile.write(';'.join(values)+"\n")

	# PrintTime('console', "\tDone")
	return coverageStats
